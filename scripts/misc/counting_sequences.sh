#!/bin/bash

function getcounts {
        echo -e "Gene\tGuide_sequence\tIll_read_counts\tIT_reads_counts" > $out_dir/counts/$3\_guide_sequences_counts.txt
        mycounts=0
        while IFS= read -r line; do
                data=(${line//\t/ })
                ill_count=$(grep -m 1 ${data[1]} $seq_file | awk '{print $3}')
                mycounts=0
                for seq in ${data[@]:2}; do
                        counts=$(zgrep $seq $fastq/$2 | wc -l)
                        mycounts=$(( mycounts + counts ))
                        comp=$(echo ">seq1 $seq" | tr ' ' '\n' | seqkit seq --quiet -t dna -r -p | tr '\n' ' ' | awk '{print $2}')
                        counts=$(zgrep $comp $fastq/$2 | wc -l)
                        mycounts=$(( mycounts + counts ))
                done
                echo -e "${data[0]}\t${data[1]}\t$ill_count\t$mycounts" >> $out_dir/counts/$3\_guide_sequences_counts.txt
        done <<< `grep -v ^Gene $1`
}

export -f getcounts

function countseq {
        declare -a COUNT_files=()
        for file in $(ls $fastq); do
                echo $file > /dev/stderr
                name=$(echo $file | sed -e 's/\./ /g' | awk '{print $2}')
                echo $name > /dev/stderr
                COUNT_file="$(getcounts $1 $file $name)" &
                COUNT_files=(${COUNT_files[@]} $COUNT_file)
        done
        echo COUNT_files
}

function gettarget {
        myseqs=$(echo $1 | sed -e 's/AAA\+/ /g' -e 's/CCC\+/ /'g -e 's/GGG\+/ /g' -e 's/TTT\+/ /g')
        targets=(${myseqs// / })
        target=''
        if (( ${#targets[@]} > 1 )); then
                for seq in ${targets[@]}; do
                        if (( ${#seq} > ${#target} )); then
                                target="$seq"
                        fi
                done
        else 
                target=${targets[0]}
        fi
        echo $target
}

function gettargets {
        echo "Gene Guide Target Repeats" > $out_dir/norepeats.txt
        echo "Gene Guide Target Repeats" > $out_dir/repeats.txt
        SEQs_file="$out_dir/seq2look.txt"
        echo 
        if [ -f "$SEQs_file" ] && (( $(wc -l $SEQs_file | awk -v col="1" '{print $col}') == $(wc -l $1 | awk -v col="1" '{print $col}') )); then
                echo $SEQs_file
        else 
                echo "Gene Guide Targets" > $SEQs_file
                while IFS= read -r line; do
                        declare -a finalseqs=()
                        data=(${line//\t/ })
                        gene=${data[0]}
                        guide=${data[1]}
                        ill_count=${data[2]}
                        target=$(gettarget $guide)   
                        repeats=$(grep $target $seq_file | wc -l)     
                        if (( repeats > 1 )); then
                                echo $gene $guide $target $repeats >> $out_dir/repeats.txt
                                finalseqs[0]="$guide"
                                finalseqs[1]="$(echo $guide | sed -e 's/AAA/AA/g' -e 's/CCC/CC/'g -e 's/GGG/GG/g' -e 's/TTT/TT/g' -e 's/AAAA/AAA/g' -e 's/CCCC/CCC/'g -e 's/GGGG/GGG/g')"
                                finalseqs[2]="$(echo $guide | sed -e 's/AAA/AAAA/g' -e 's/CCC/CCCC/'g -e 's/GGG/GGGG/g' -e 's/TTT/TTTT/g' )" 
                        else
                                echo $gene $guide $target $repeats >> $out_dir/norepeats.txt
                                finalseqs[0]="$target"
                        fi
                        echo "$gene $guide ${finalseqs[@]}" >> $SEQs_file
                done <<< $(grep -v ^Gene $1)
                echo $SEQs_file
        fi
}

help="counting_sequeces.sh -f fastq_dir (required) -s sequences_file (required) -o output_dir (required) -c threads (default 20)"

threads=20
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -f) fastq=$2
        shift;;
    -s) seq_file=$2
	shift;;
    -o) out_dir=$2
        shift;;
    -c) threads=$2
	shift;;
    -h) echo $help ;;
    --) shift
        break ;;
    *) echo "Option $1 not recognized" ;;
    esac
    shift
done

if [[ ! $fastq || ! $out_dir || ! $seq_file ]]
        then
                echo $help
                exit
fi


currentDate=`date`
echo "Starting analysis... $currentDate"
echo "Fastq dir = $fastq"
echo "Sequences = $seq_file"
echo "Output folder = $out_dir"

if [ ! -d "$out_dir" ]
then
        mkdir -p $out_dir
else
	rm -rf $out_dir/counts
        mkdir -p $out_dir/counts
fi

targets="$(gettargets $seq_file)"
#targets="/home/jmrosa/Documents/Projects/Colaboraciones/GdeCarcer/Results/Run558/seq2look.txt"
echo $targets
output=s"$(countseq $targets)"
echo $outputs
exit

transfq="$(transponse $out_dir/se2pe/trans $fastq)"
splittedfq="$(splitfq $out_dir/se2pe/split $transfq)"
echo $splittedfq
#createpe $out_dir/fastqs $splittedfq ${name}_$n
outfiles="$(createpe $out_dir/se2pe/fastqs $splittedfq ${name}_$n)"
echo $outfiles
jointedfq="$(jointfq $out_dir $out_dir/se2pe/fastqs $outfiles $name)"
echo $jointedfq

currentDate=`date`
echo "Analysis finished... $currentDate"



