#!/usr/bin/env python
"""
SYNOPSIS
    counting_vars_bed.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to count variants within regions
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    counting_vars_bed.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import re
import os
from tools import _gettime_

def _writeoutput (info=None, file=None):
    _path = '/'.join(file.split('/')[:-1])
    if not os.path.exists(_path):
        os.makedirs(_path)
    with open(file, 'w') as f:
        _myfields = ["#Gene","Consequence","In","Out", "%In"]
        f.write(';'.join(_myfields) + "\n")
        for gene in info["genes"]:
            for consequence in info["genes"][gene]:
                _mypercent = 0
                if info["genes"][gene][consequence]["in"] > 0 and info["genes"][gene][consequence]["out"] > 0:
                    _mytotal = info["genes"][gene][consequence]["in"] + info["genes"][gene][consequence]["out"]
                    _mypercent = "{0:.2f}".format(info["genes"][gene][consequence]["in"] / float(_mytotal))
                elif info["genes"][gene][consequence]["in"] > 0 and info["genes"][gene][consequence]["out"] == 0:
                    _mypercent = 1
                _myfields = [gene,consequence,str(info["genes"][gene][consequence]["in"]),str(info["genes"][gene][consequence]["out"]), str(_mypercent)]
                f.write(';'.join(_myfields) + "\n")
    return 0

def _getpositions(file=None):
    positions = {}
    with open(file, 'r') as f:
        for line in f:
            line = line[:-1]
            if re.match('chr', line) != None:
                _mychr = line.split('\t')[0].replace('chr', '')
                _mystart = int(line.split('\t')[1])
                _myend = int(line.split('\t')[2])
                if _mychr not in positions.keys():
                    positions[_mychr] = {}
                for position in range(_mystart, _myend + 1):
                    positions[_mychr][position] = 'checked'
    return positions

def _getvariants(file=None):
    variants = []
    with open(file, 'r') as f:
        for line in f:
            line = line[:-1]
            if re.match('Hugo', line) == None:
                variant = {}
                variant["chr"] = line.split('\t')[4]
                variant["pos1"] = line.split('\t')[5]
                variant["pos2"] = line.split('\t')[6]
                variant["consequence"] = line.split('\t')[8]
                variant["gene"] = line.split('\t')[0]
                variants.append(variant)
    return variants

def _getsummary(bed=None, var=None):
    _mypositions = _getpositions(file=bed)
    _summary = {"genes":{}, "total":0}
    for variant in _getvariants(file=var):
        _summary["total"] += 1
        if variant["gene"] not in _summary["genes"].keys():
            _summary["genes"][variant["gene"]] = {}
        if variant["chr"] in _mypositions.keys() and (int(variant["pos1"]) in _mypositions[variant["chr"]].keys() or int(variant["pos2"]) in _mypositions[variant["chr"]].keys()):
            if variant["consequence"] not in _summary["genes"][variant["gene"]].keys():
                _summary["genes"][variant["gene"]][variant["consequence"]] = {}
                _summary["genes"][variant["gene"]][variant["consequence"]]["out"] = 0
                _summary["genes"][variant["gene"]][variant["consequence"]]["in"] = 1
            else:
                _summary["genes"][variant["gene"]][variant["consequence"]]["in"] += 1
        else:
            if variant["consequence"] not in _summary["genes"][variant["gene"]].keys():
                _summary["genes"][variant["gene"]][variant["consequence"]] = {}
                _summary["genes"][variant["gene"]][variant["consequence"]]["in"] = 0
                _summary["genes"][variant["gene"]][variant["consequence"]]["out"] = 1
            else:
                _summary["genes"][variant["gene"]][variant["consequence"]]["out"] += 1
    return _summary

def _main_(argv):
    if  not argv:
        print 'counting_vars_bed.py -b <bed file> -i <var file> -o <output file>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hb:i:o:")
    except getopt.GetoptError:
        print 'counting_vars_bed.py -b <bed file> -i <var file> -o <output file>'
        sys.exit(2)

    except getopt.GetoptError:
        print 'counting_vars_bed.py -b <bed file> -i <var file> -o <output file>'
        sys.exit(2)

    bed_file, var_file, output_file= '', '', ''
    for opt, arg in opts:
        if opt == '-h':
            print 'counting_vars_bed.py -b <bed file> -i <var file> -o <output file>'
            sys.exit()
        elif opt == '-b':
            bed_file = arg
        elif opt == '-i':
            var_file = arg
        elif opt == '-o':
            output_file = arg
    if bed_file == '' or var_file == '':
        print "<<ERROR>> not valid input file"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _mysummary = _getsummary(bed=bed_file, var=var_file)
    if _writeoutput (info=_mysummary, file=output_file) == 0:
        print "Process finished... " + _gettime_()
        sys.exit(0)
    else:
        sys.exit(1)

_main_(sys.argv[1:])