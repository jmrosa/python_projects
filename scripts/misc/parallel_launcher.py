#!/usr/bin/env python
"""
SYNOPSIS
    parallel_launcher.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    parallel_launcher.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import re
import pp
from tools import *

def _help ():
        print "####################"
        print "parallel_launcher.py v0.1"
        print 'parallel_launcher.py -f <file> -t <number of threads>'
        print "####################"

def _getcommands(file=None):
    _commands = []
    with open(file, 'r') as f:
        for line in f:
            line = re.sub(r'\n', '', line)
            _commands.append(line)
    return _commands

def _launchcommands(commands=None, threads=None):
    ppservers = ('193.145.10.201:443',)
    job_server = pp.Server(int(threads), ppservers=ppservers)
    print "Starting pp with", job_server.get_ncpus(), "workers"
    jobs = []
    for command in commands:
         jobs.append(job_server.submit(_command_, (command,),(),("subprocess",)))
    for job in jobs:
        print "Executing command %s" % job
        result = job()
        print result
    job_server.wait()
    job_server.print_stats()

def _main_(argv):
    if not argv:
        _help()
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hf:t:")

    except getopt.GetoptError:
        _help()
        sys.exit(2)

    file, threads = '', ''
    for opt, arg in opts:
        if opt == '-h':
            _help()
            sys.exit()
        elif opt == '-f':
            file = arg
        elif opt == '-t':
            threads = arg
    if file == '' or threads == '':
        print "<<ERROR>> not enough commands"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _mycommands = _getcommands(file=file)
    _launchcommands(_mycommands, threads=threads)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
