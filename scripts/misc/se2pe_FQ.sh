#!/bin/bash

function splitfq {
        mkdir -p $1
        prefix="splitted_"
	myid=`head -n 1 $2 | awk -F":" '{print $1}'`
	lines=`grep $myid $2 | wc -l`
	number=$((lines / 20))
        `split -l $number $2 $1/$prefix`
        echo "$1/${prefix}"
}

function se2pe {
        while IFS= read -r line
        do
                data=(${line//\t/ })
                echo -e "${data[0]} 1:N:0:11\n${data[1]:0:$(( `echo ${data[1]} | wc -c` / 2 ))}\n+\n${data[3]:0:$(( `echo ${data[3]} | wc -c` / 2 ))}" | seqkit seq --quiet -t dna | gzip -c >> $1/${3}_R1.fastq.gz
                echo -e "${data[0]} 2:N:0:11\n${data[1]:$(( `echo ${data[1]} | wc -c` / 2 )):`echo ${data[1]} | wc -c`}\n+\n${data[3]:$(( `echo ${data[3]} | wc -c` / 2 )):`echo ${data[3]} | wc -c`}" | seqkit seq --quiet -t dna -r -p | gzip -c  >> $1/${3}_R2.fastq.gz
        done < "$2"
}

export -f se2pe

function createpe {
	currentDate=`date`
	echo "Starting se2pe... $currentDate"
	mkdir -p $1
	declare -a PEfiles
	mythreads=$((threads - 1))
	n=1
	for file in `ls $2*`; do
        	PEfiles+=${name}_$n
		#sem -j+$mythreads "se2pe $1 $file ${name}_$n"
		PE="$(se2pe $1 $file ${name}_$n)" &
        	n=$((n+1))
		
	done
	#sem --wait
	wait
	echo $PEfiles
}
	
function transponse {
        mkdir -p $1
	file="$1/transponse.fastq"
	myid=`gunzip -c $2 | head -n 1 | awk -F":" '{print $1}'`
	`gunzip -c $2 | grep -A3 $myid | sed -e "s/$myid/\s$myid/" | tr '\n' '\t' | tr '\s' '\n' | sed -r '/^\s*$/d' > $file`
        echo $file
}

function jointfq {
        R1_file="$1/${name}_R1.fastq.gz"
	R2_file="$1/${name}_R2.fastq.gz"
        cat $out_dir/se2pe/fastqs/*R1.fastq.gz > $R1_file
	cat $out_dir/se2pe/fastqs/*R2.fastq.gz > $R2_file
	echo -e "$R1_file\t$R2_file"
}


help="se2pe_FQ.sh -f fastq_file (required) -n output_name (required) -o output_dir (required) -c threads (default 20)"

threads=20
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -f) fastq=$2
        shift;;
    -n) name=$2
	shift;;
    -o) out_dir=$2
        shift;;
    -c) threads=$2
	shift;;
    -h) echo $help ;;
    --) shift
        break ;;
    *) echo "Option $1 not recognized" ;;
    esac
    shift
done

if [[ ! $fastq || ! $out_dir || ! $name ]]
        then
                echo $help
                exit
fi


currentDate=`date`
echo "Starting analysis... $currentDate"
echo "Fastq file = $fastq"
echo "Name = $name"
echo "Output folder = $out_dir"

if [ ! -d "$out_dir/se2pe/log" ]
then
        mkdir -p $out_dir/se2pe/log
else
	rm -rf $out_dir/se2pe
	mkdir -p $out_dir/se2pe/log
fi

if [ -e "${outdir}/${name}_R1.fastq.gz" ]
then
	rm ${outdir}/${name}_R?.fastq.gz
fi

transfq="$(transponse $out_dir/se2pe/trans $fastq)"
splittedfq="$(splitfq $out_dir/se2pe/split $transfq)"
echo $splittedfq
#createpe $out_dir/fastqs $splittedfq ${name}_$n
outfiles="$(createpe $out_dir/se2pe/fastqs $splittedfq ${name}_$n)"
echo $outfiles
jointedfq="$(jointfq $out_dir $out_dir/se2pe/fastqs $outfiles $name)"
echo $jointedfq

currentDate=`date`
echo "Analysis finished... $currentDate"



