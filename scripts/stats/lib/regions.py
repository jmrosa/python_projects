#!/usr/bin/env python
"""
SYNOPSIS
    regions.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Module for coverage statistics
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    regions.py v0.1
"""
__author__ = 'jmrosa'

import sys
import re
from scipy import stats
import numpy
from tools import *

# Class definitions
class Region(object):
    """Class essay to create essay objects"""
    def __init__(self, info=None):
        self.gene = info.split("\t")[3]
        self.chr = info.split("\t")[0]
        self.start = int(info.split("\t")[1])
        self.end = int(info.split("\t")[2])
        self.length = (self.end - self.start)

    def _getinfo(self, cov=None, count=None):
        self._getdepth(cov_hash=cov)
        self._getcount(count_hash=count)

    def _getdepth (self, cov_hash=None):
        _mydepth = []
        for transcript in cov_hash[str(self.chr)][str(self.start)][str(self.end)].keys():
            for base in range(1, len(cov_hash[str(self.chr)][str(self.start)][str(self.end)][str(transcript)].keys()) + 1):
                _mydepth.append(int(cov_hash[str(self.chr)][str(self.start)][str(self.end)][str(transcript)][str(base)]))

        ### Calculating global statistics ###
        self.meandp = "{0:.2f}".format(numpy.nanmean(_mydepth))
        self.mediandp = "{0:.2f}".format(numpy.nanmedian(_mydepth))
        self.maxdp = max(_mydepth)
        self.mindp = min(_mydepth)
        self.zeros = int(_mydepth.count(0))
        self.zeros_fraction = "{0:.2f}".format(float(_mydepth.count(0))/len(_mydepth))
        self.twohundreds = "{0:.2f}".format(sum(i > 199 for i in _mydepth)/float(len(_mydepth)))

        ### Calculating quartile statistics ###
        _myfq = int(len(_mydepth)/4)
        _mylq = int(len(_mydepth)*float(0.75))
        if (_myfq > 0 and _mylq == 0):
            ### First Quartile ###
            self.firstquartile_meandp = "{0:.2f}".format(numpy.nanmean(_mydepth[:_myfq]))
            self.firstquartile_meadiandp = "{0:.2f}".format(numpy.nanmedian(_mydepth[:_myfq]))
            self.firstquartile_zeros = "{0:.2f}".format(float(_mydepth[:_myfq].count(0))/_myfq)

            ### Second and Third Quartile ###
            self.middle_meandp = "{0:.2f}".format(numpy.nanmean(_mydepth[_myfq:_mylq]))
            self.middle_meadiandp = "{0:.2f}".format(numpy.nanmedian(_mydepth[_myfq:_mylq]))
            self.middle_zeros = "{0:.2f}".format(float(_mydepth[_myfq:_mylq].count(0))/len(_mydepth[_myfq:_mylq]))

            ### Fourth Quartile ###
            self.lastquartile_meandp = "{0:.2f}".format(numpy.nanmean(_mydepth[_mylq:]))
            self.lastquartile_meadiandp = "{0:.2f}".format(numpy.nanmedian(_mydepth[_mylq:]))
            self.lasstquartile_zeros = "{0:.2f}".format(float(_mydepth[_mylq:].count(0))/_mylq)
        else:

            ### First Quartile ###
            self.firstquartile_meandp = self.meandp
            self.firstquartile_meadiandp = self.mediandp
            self.firstquartile_zeros = self.zeros_fraction

            ### Second and Third Quartile ###
            self.middle_meandp = self.meandp
            self.middle_meadiandp = self.mediandp
            self.middle_zeros = self.zeros_fraction

            ### Fourth Quartile ###
            self.lastquartile_meandp = self.meandp
            self.lastquartile_meadiandp = self.mediandp
            self.lasstquartile_zeros = self.zeros_fraction

    def _getcount (self, count_hash=None):
        for transcript in count_hash[str(self.chr)][str(self.start)][str(self.end)].keys():
            self.count = count_hash[str(self.chr)][str(self.start)][str(self.end)][transcript]


