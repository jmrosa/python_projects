#!/usr/bin/env python
"""
SYNOPSIS
    coverage_stats.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to calculate stats from coverage
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    coverage_stats.py v0.1
"""
__author__ = 'jmrosa'

import sys
import os
import re
import getopt
from scipy import stats
import re
from numpy import *
from regions import Region
from tools import *

def _printregions (file=None, regions=None):
    for region in regions:
        _myinfo = ';'.join([region.gene, region.chr, str(region.start), str(region.end), str(region.length), str(region.zeros_fraction),str(region.meandp), str(region.mediandp), str(region.maxdp), str(region.mindp), str(region.firstquartile_zeros), str(region.firstquartile_meandp), str(region.firstquartile_meadiandp), str(region.middle_zeros), str(region.middle_meandp), str(region.middle_meadiandp), str(region.lasstquartile_zeros), str(region.lastquartile_meandp), str(region.lastquartile_meadiandp)])
        _appendfile_(file=file, info=_myinfo + '\n')

def _printheader (file=None):
    _myinfo = ';'.join(["#Gene", "Chr", "Start", "End", "Length","%Zeros", "Mean_Depth", "Median_Depth", "Max", "Min", "FQ_%Zeros", "FQ_Mean_Depth", "FQ_Median_Depth", "MQ_%Zeros", "MQ_Mean_Depth", "MQ_Median_Depth", "LQ_%Zeros", "LQ_Mean_Depth", "LQ_Median_Depth"])
    _createfile_(file=file, info=_myinfo + '\n')

def _getglobal(regions=None):
    _mydata = {"meandp":[],"mediandp":[],"maxdp":[],"mindp":[],"zeros_fraction":[],"firstquartile_meandp":[],"firstquartile_meadiandp":[],"firstquartile_zeros":[],"middle_meandp":[],"middle_meadiandp":[],"middle_zeros":[],"lastquartile_meandp":[],"lastquartile_meadiandp":[],"lasstquartile_zeros":[]}
    _length = 0
    for region in regions:
        _length += region.length
        _mydata["meandp"].append(float(region.meandp))
        _mydata["mediandp"].append(float(region.mediandp))
        _mydata["maxdp"].append(float(region.maxdp))
        _mydata["mindp"].append(float(region.mindp))
        _mydata["zeros_fraction"].append(float(region.zeros_fraction))
        _mydata["firstquartile_meandp"].append(float(region.firstquartile_meandp))
        _mydata["firstquartile_meadiandp"].append(float(region.firstquartile_meadiandp))
        _mydata["firstquartile_zeros"].append(float(region.firstquartile_zeros))
        _mydata["middle_meandp"].append(float(region.middle_meandp))
        _mydata["middle_meadiandp"].append(float(region.middle_meadiandp))
        _mydata["middle_zeros"].append(float(region.middle_zeros))
        _mydata["lastquartile_meandp"].append(float(region.lastquartile_meandp))
        _mydata["lastquartile_meadiandp"].append(float(region.lastquartile_meadiandp))
        _mydata["lasstquartile_zeros"].append(float(region.lasstquartile_zeros))

    _medians = {}
    for key in _mydata.keys():
        _medians[key] = "{0:.2f}".format(stats.nanmedian(_mydata[key]))
        _medians[key] = "{0:.2f}".format(median(_mydata[key]))
        _medians["length"] = _length
    _global=Region('\t'.join(["0","0","0","All"]))
    _global.__dict__.update(_medians)
    return _global

def _getregions(bed=None, cov=None, count=None):
    """Getting a list of region objects from the bed file"""
    try:
        _myregions = []
        with open(bed, "r") as f:
            for line in f:
                line = line[:-1]
                if re.match("chr", line) != None:
                    _myregion = Region(line)
                    _myregion._getinfo(cov=cov, count=count)
                    _myregions.append(_myregion)
        return _myregions

    except IOError as e:
        print '<<ERROR>> Can\'t open file ' + e.strerror + ' ' + _gettime_()
        sys.exit(1)

def _launchcommand (command=None):
    _myexit = _command_(command)
    if _myexit != 0:
        print "<<ERROR>> %s can\'t be executed" % (command)
        sys.exit(1)

def _getcoverage(bed=None, bam=None, tmp=None):
    """Getting a list of region objects from the bed file"""
    _myname = bam.split('/')[-1]
    _myname = _myname.split('.')[0]
    files = [tmp + "/" + _myname + '_tmp.cov', tmp + "/" + _myname + '_tmp.count']
    # _launchcommand('coverageBed -abam %s -b %s -d > %s' % (bam, bed, files[0]))
    # _launchcommand('coverageBed -abam %s -b %s -counts > %s' % (bam, bed, files[1]))
    print "Starting cov hash... " + _gettime_()
    _mycov = _create_hash_from_file_(file=files[0])
    print "Finished cov hash and starting count hash... " + _gettime_()
    _mycount = _create_hash_from_file_(file=files[1])
    print "Finished count hash... " + _gettime_()
    # os.remove(files[0])
    # os.remove(files[1])
    return _mycov, _mycount

def _main_(argv):
    if not argv:
        print 'coverage_stats.py -i <bam file> -b <bed file> -o <output name> -d <tmp dir>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hi:b:o:d:")

    except getopt.GetoptError:
        print 'coverage_stats.py -i <bam file> -b <bed file> -o <output name> -d <tmp dir>'
        sys.exit(2)

    bam_file, bed_file, output_name, temp_dir= '', '', '', ''
    for opt, arg in opts:
        if opt == '-h':
            'coverage_stats.py -i <bam file> -b <bed file> -o <output name> -d <tmp dir>'
            sys.exit()
        elif opt == '-i':
            bam_file = arg
        elif opt == '-b':
            bed_file = arg
        elif opt == '-o':
            output_name = arg
        elif opt == '-d':
            temp_dir = arg
    if bam_file == '' or bed_file == '':
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    cov_hash, count_hash = _getcoverage(bed=bed_file, bam=bam_file, tmp=temp_dir)
    _myregions = _getregions(bed=bed_file, cov=cov_hash, count=count_hash)
    _myregions.append(_getglobal(_myregions))
    _myoutput = output_name + '_regions.txt'
    _printheader(file=_myoutput)
    _printregions(regions=_myregions, file=_myoutput)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
