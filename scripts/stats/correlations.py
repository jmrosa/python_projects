#!/usr/bin/env python
"""
SYNOPSIS
    correlations.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    correlations.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import re
import rpy2.robjects as robjects
import rpy2.robjects.numpy2ri as rpyn
import rpy2.rinterface
from rpy2.robjects.vectors import DataFrame
from rpy2.robjects.packages import importr
from tools import *

def _help ():
        print "####################"
        print "correlations.py v0.1"
        print 'correlations.py -a <file> -b <b file> -o <root name>'
        print "- Samples must be placed in columns, rows for data -"
        print "####################"

def _getalldata(a=None, b=None):
    _mydata = {"header":[], "compound": {}, "numbers": {}}
    _order = {"samples":{}}
    a_names = []
    b_names = []
    with open(a, 'r') as f:
        n = 0
        for line in f:
            line = re.sub(r'\r\n', '', line)
            if n == 0:
                _mydata["header"].extend(line.split("\t")[1:])
                i = 1
                for sample in line.split("\t")[1:]:
                    _mydata["numbers"][sample] = i
                    i += 1
            else:
                _rowname = line.split("\t")[0]
                a_names.append(_rowname)
                _mydata["compound"][_rowname] = ",".join(line.split("\t")[1:])
            n += 1

    with open(b, 'r') as f:
        n = 0
        for line in f:
            line = re.sub(r'\r\n', '', line)
            if n == 0:
                i = 1
                for sample in line.split("\t")[1:]:
                    _order["samples"][i] = _mydata["numbers"][sample]
                    i += 1
            else:
                _rowname = line.split("\t")[0]
                b_names.append(_rowname)
                _line = []
                for i in range(1, len(line.split("\t")[1:])+1):
                    _line.insert(_order["samples"][i], line.split("\t")[i])
                _mydata["compound"][_rowname] = ",".join(_line)
            n += 1

    _mydataframe = {"compound":{}, "header":_mydata["header"]}

    for compound in _mydata["compound"]:
        _mydataframe["compound"][compound] = robjects.FloatVector(_mydata["compound"][compound].split(","))

    _dataframe = robjects.DataFrame(_mydataframe["compound"])
    _dataframe.rownames = _mydataframe["header"]
    return a_names, b_names, _dataframe

def _getcorrelations (frame=None, name=None):
    _cor = robjects.r['cor.test']
    _myorder = {}
    i=0
    for fcol in range(len(frame.colnames)):
        if i == 0:
            _header = []
            _header.insert(0,"Name")
        _myinfocor = ["-" for x in range(1, len(frame.colnames))]
        _myinfopvalue = ["-" for x in range(1, len(frame.colnames))]
        _myinfocor.insert(0, frame.colnames[fcol])
        _myinfopvalue.insert(0, frame.colnames[fcol])
        j=1
        for scol in range(i, len(frame.colnames)):
            try:
                _res = _cor(frame[fcol], frame[scol], method="spearman")
                _pvalue = "{0:.2f}".format(rpyn.ri2numpy(_res[2])[0])
                _rho = "{0:.2f}".format(rpyn.ri2numpy(_res[3])[0])
            except rpy2.rinterface.RRuntimeError:
                _pvalue = "NA"
                _rho = "NA"
            if i == 0:
                _myorder[frame.colnames[scol]] = j
                _header.insert(_myorder[frame.colnames[scol]], frame.colnames[scol])
            _myinfocor.insert(_myorder[frame.colnames[scol]], _rho)
            _myinfopvalue.insert(_myorder[frame.colnames[scol]], _pvalue)
            j += 1

        if i == 0:
            _createfile_(file=name + "_cor.txt", info=";".join(_header) + '\n')
            _createfile_(file=name + "_pvalues.txt", info=";".join(_header) + '\n')
        _appendfile_(file=name + "_cor.txt", info=";".join(map(str,_myinfocor)) + '\n')
        _appendfile_(file=name + "_pvalues.txt", info=";".join(map(str, _myinfopvalue)) + '\n')
        i += 1
    return 0

def _printresults(data=None, name=None):
    n = 0
    _myorder = {}
    for fcol in data:
        if n == 0:
            _header = []
            _header.insert(0,"Name")
        _myinfocor = []
        _myinfopvalue = []
        _myinfocor.insert(0, fcol)
        _myinfopvalue.insert(0, fcol)
        i = 1
        for scol in data:
            if n == 0:
                _myorder[scol] = i
                _header.insert(_myorder[scol], scol)
            _myinfocor.insert(_myorder[scol], data[fcol][scol]["rho"])
            _myinfopvalue.insert(_myorder[scol], data[fcol][scol]["pvalue"])
            i += 1
        if n == 0:
            _createfile_(file=name + "_cor.txt", info=";".join(_header) + '\n')
            _createfile_(file=name + "_pvalues.txt", info=";".join(_header) + '\n')
        _appendfile_(file=name + "_cor.txt", info=";".join(map(str,_myinfocor)) + '\n')
        _appendfile_(file=name + "_pvalues.txt", info=";".join(map(str, _myinfopvalue)) + '\n')
        n += 1

def _printsummary(a=None, b=None, name=None):
    _names = [name + "_cor.txt", name + "_pvalues.txt"]
    #_names = [name + "_pvalues.txt"]
    for _name in _names:
        _output = re.sub(r'.txt', '', _name) + "_summary.txt"
        _createfile_(file=_output, info=";".join(["SNP","Compound","Value","Freq"]) + '\n')
        _data = []
        _myorder = {}
        with open(_name, 'r') as f:
            n = 0
            for line in f:
                line = re.sub(r'\n', '', line)
                _data.append(line.split(";"))
                if n == 0:
                    for number in range(len(line.split(";"))):
                        _myorder[line.split(";")[number]] = number
                n += 1
            for snp1 in b:
                _values = []
                for snp2 in b:
                    if int(_myorder[snp1]) < int(_myorder[snp2]): _values.append(_data[_myorder[snp1]][_myorder[snp2]])
                    else: _values.append(_data[_myorder[snp2]][_myorder[snp1]])

                for compound in a:
                    if int(_myorder[snp1]) < int(_myorder[compound]): _value = _data[_myorder[snp1]][_myorder[compound]]
                    else: _value = _data[_myorder[compound]][_myorder[snp1]]
                    if float(_value) > 0: _total = sum(1 for x in _values if float(x) >= float(_value))
                    else: _total = sum(1 for x in _values if float(x) <= float(_value))
                    _appendfile_(file=_output, info=";".join([snp1, compound, _value, "{0:.4f}".format(float(_total)/len(_values))]) + '\n')

def _main_(argv):
    if not argv:
        _help()
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"ha:b:o:")

    except getopt.GetoptError:
        _help()
        sys.exit(2)

    a_file, b_file, output = '', '', ''
    for opt, arg in opts:
        if opt == '-h':
            _help()
            sys.exit()
        elif opt == '-a':
            a_file = arg
        elif opt == '-b':
            b_file = arg
        elif opt == '-o':
            output = arg
    if a_file == '' or b_file == '':
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    comp_a, comp_b, _mydataframe = _getalldata(a=a_file, b=b_file)
    #_getcorrelations(frame = _mydataframe, name=output)
    #_printresults(data=_mycorrelations, name=output)
    _printsummary(a=comp_a, b=comp_b, name=output)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
