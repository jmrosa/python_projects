#!/usr/bin/env Rscript
packages<-c("stringi", "stringr","Biostrings","XML","biomaRt","rtracklayer","ShortRead","optparse")
#suppressWarnings(update.packages(ask = FALSE, dependencies = c('Suggests')))
if (!requireNamespace("BiocManager", quietly = TRUE)){
  install.packages("BiocManager")
}

if(as.numeric(as.character(BiocManager::version()))  < 3.9) {
  BiocManager::install(version = '3.9', ask= FALSE)
}

for (package in packages){
  if (!requireNamespace(package, quietly = TRUE)){
    cat("Installing package", package, "and dependencies...\n")
    suppressMessages(BiocManager::install(package, dependencies = TRUE))
  }
}

options(java.parameters = "-Xmx15G")
suppressMessages(suppressWarnings(invisible(lapply(packages, library, character.only = TRUE, quietly = TRUE))))


#################################

split_se_data <- function(fastq, forward, reverse) {
  reads <- readDNAStringSet(fastq, format="fastq", with.qualities=TRUE)
  quals <- IlluminaQuality(mcols(reads)$qualities)
  for (n in 1:length(reads)){
  # for (n in c(1)){
    myid <- as.character(names(as.list(reads[n])))
    myseq <- as.character(reads[[as.character(names(as.list(reads[n])))]])
    myfseq <- DNAString(substr(myseq, 1, ceiling(nchar(myseq)/2)))
    myrseq <- reverseComplement(DNAString(substr(myseq, ceiling((nchar(myseq)/2)+1), nchar(myseq))))
    myquals <- as.character(mcols(reads[n])$qualities)
    myfquals <- substr(myquals, 1, ceiling(nchar(myquals)/2))
    myrquals <- as.character(stri_reverse(substr(myquals, ceiling((nchar(myquals)/2)+1), nchar(myquals))))
    f_id <- paste0(myid," 1:N:0:11")
    r_id <- paste0(myid," 2:N:0:11")
    f_data <- QualityScaledDNAStringSet(DNAStringSet(myfseq), IlluminaQuality(myfquals))
    names(f_data)<- f_id
    r_data <- QualityScaledDNAStringSet(DNAStringSet(myrseq), IlluminaQuality(myrquals))
    names(r_data) <- r_id
    print_results(data=f_data, file = forward)
    print_results(data=r_data, file = reverse)
  }
  return(c(1, 1))
}

get_fastqs <- function (dir) {
  pattern <- "*.fastq*"
  if(length(list.files(file.path(as.character(dir)), pattern=glob2rx(pattern))) > 0) {
    myfastqs <- paste(file.path(as.character(dir)), list.files(file.path(as.character(dir)), pattern=glob2rx(pattern)), sep = "/")
  }
  return (myfastqs)
}

transform_se2pe <- function(file){
  myforward <- paste(opt$outdir, paste0(opt$name, "_", mydate,"_R1.fastq.gz"), sep="/")
  if(file.exists(myforward)){file.remove(myforward)}
  myreverse <- paste(opt$outdir, paste0(opt$name, "_", mydate,"_R2.fastq.gz"), sep="/")
  if(file.exists(myreverse)){file.remove(myreverse)}
  mysplits <- split_se_data(fastq = file, forward=myforward, reverse=myreverse)
  return(c(myforward,myreverse))
}

print_results <- function(data, file){
  writeQualityScaledXStringSet(data, file, append = TRUE, compress=TRUE)
}
#############################

option_list = list(
  make_option(c("-f", "--fastq"), type="character", default=NULL, 
              help="fastq files folder", metavar="character"),
  make_option(c("-n", "--name"), type="character", default=NULL, 
              help="output file name", metavar="character"),
  make_option(c("-o", "--outdir"), type="character", default="./", 
              help="output dir [default = %default]", metavar="character")
) 
opt_parser = OptionParser(option_list=option_list)
opt = parse_args(opt_parser)

if (is.null(opt$fastq) ||  is.null(opt$name)){
  print_help(opt_parser)
  stop("Input information missed", call.=FALSE)
}

cat("Process started... ", as.character(Sys.time()), "\n")
mydate <- gsub("-", '_', as.character(gsub(" ([0-9]+:[0-9]+)*" , '', as.character(Sys.time()))))

if (file.exists(opt$outdir)) { 
  setwd(file.path(opt$outdir))
}else {
  dir.create(opt$outdir)
  setwd(file.path(opt$outdir))
}

fastqs <- get_fastqs(dir=opt$fastq)
for (file in fastqs){
  results <- transform_se2pe(file = file)
  print(as.list(results[1]))
  print(as.list(results[2]))
}

cat("Process finished... ", as.character(Sys.time()), "\n")
invisible(quit())
