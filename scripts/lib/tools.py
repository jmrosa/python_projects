#!/usr/bin/env python
"""
SYNOPSIS
    tools.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Module for commonly used tools
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    tools.py v0.1
"""
__author__ = 'jmrosa'

from datetime import datetime
import re
import os
import sys
import subprocess
import pp

__all__ = ['_create_hash_from_file_', '_gettime_', '_addkeys_', '_getmatch_', '_replace_', '_createfile_', '_command_', '_appendfile_', '_parallel_','_commandout_']

def _create_hash_from_file_(file=None):
    try:
        _myhash = {}
        with open(file, 'r') as data:
            for line in data:
                line = line.replace("\r", "").replace("\n", "")
                _mydata = line.split('\t')
                _addkeys_(_myhash, _mydata, value="yes")
        return _myhash

    except IOError as e:
        print '<<ERROR>> Can\'t open file ' + e.strerror + ' ' + _gettime_()
        sys.exit(1)

def _command_(command=None):
    _process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    #print command
    while _process.poll() is None:
        _process.communicate()
    else:
        return _process.returncode #, out, err

def _commandout_(command=None):
    _process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = _process.communicate()
    return str(out)

def _parallel_(commands=None, threads=None):
    ppservers = ('192.168.226.32:22',)
    job_server = pp.Server(int(threads), ppservers=ppservers)
    jobs = []
    for command in commands:
         jobs.append(job_server.submit(_command_, (command,),(),("subprocess",)))
    results = []
    for job in jobs:
        results.append(job())
    job_server.wait()
    return results

def _appendfile_(file=None, info=None):
    try:
        _path = '/'.join(file.split('/')[:-1])
        if not os.path.exists(_path):
            os.makedirs(_path)
        with open(file, "a+") as f:
            f.write(info)

    except IOError as e:
        print '<<ERROR>> Can\'t write to file ' + e.strerror + ' ' + _gettime_()
        sys.exit(1)

def _createfile_(file=None, info=None):
    try:
        _path = '/'.join(file.split('/')[:-1])
        if not os.path.exists(_path):
            os.makedirs(_path)
        with open(file, "w") as f:
            f.write(info)

    except IOError as e:
        print '<<ERROR>> Can\'t write to file ' + e.strerror + ' ' + _gettime_()
        sys.exit(1)

def _gettime_(type = "classic"):
    time = datetime.now()
    dates = {}
    dates["classic"] = str(time.day) +  "/" + str(time.month) + "/" + str(time.year) +  "  " + str(time.hour) + ":" + str(time.minute) + ":" + str(time.second)
    dates["files"] = str(time.year) + str(time.month) + str(time.day)

    return dates[type]

def _addkeys_(self = None, keys = None, value = None):
    _mydict = {}
    if self is not None: _mydict = self
    # print keys
    key = keys.pop(0)
    if key not in _mydict.keys():
        if len(keys) > 0:
            _mydict[str(key)] = _addkeys_(keys = keys, value=value)
        else:
            if value is not None:
                _mydict = key
            else:
                _mydict[str(key)] = {}
    else:
        if len(keys) > 0:
            _mydict[str(key)].update(_addkeys_(self=_mydict[str(key)], keys = keys, value = value))
    return _mydict

def _getmatch_(match=None, text=None, group=2):
    _tmp = re.match(r'(.*)\s+(.*)%s(.*)' %match, text, flags=0)
    if str(_tmp) != 'None':
        _match = _tmp.group(group)
        return _match
    else: return str(_tmp)

def _replace_(match=None, text=None, replace=''):
    return re.sub(r'%s' %match, replace, text)




