#!/usr/bin/env python
"""
SYNOPSIS
    merging_var_anno.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    merging_var_anno.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import os
from tools import _gettime_
from variants_rata import _getvariants

def _printsummary(info=None, file=None):
    _output = '.'.join(file.split('.')[:-1]) + '_filtered.txt'
    with open(_output, 'w') as f:
        _myfields = ["#Symbol","Chr","Position","RefAllele","VarAllele","ID","Number_Samples","Samples","Consequence"]
        f.write(';'.join(_myfields) + "\n")
        for variant in info:
            if variant.isdeleterious == 1:
                _mysamples, _mynumber =  [],0
                for sample in range(len(variant.samples)):
                    if variant.samples[sample].Depth > 20 and variant.samples[sample].Ratio != '-' and variant.samples[sample].Ratio > 0.01:
                        _mysamples.append(variant.samples[sample].name)
                        _mynumber += 1
                if _mynumber > 0:
                    f.write(';'.join([variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation,str(_mynumber),'&'.join(_mysamples),variant.Consequence]) + "\n")

def _printheader (header=None, file=None):
        _path = '/'.join(file.split('/')[:-1])
        if not os.path.exists(_path):
            os.makedirs(_path)
        _myfields = ["#Symbol","Chr","Position","RefAllele","VarAllele","ID"]
        for sample in header.split('\t')[9:]:
            _myfields.extend([sample + "_genotype",sample + "_depth",sample + "_ratio",sample + "_sbref",sample + "_sbvar"])
        _myfields.extend(["Gene","Transcript","Consequence","cDNA_pos","Codons","Prot_pos","AminoAcids","Clinical","Sift","Pubmed","GMAF"])
        with open(file, 'w') as f:
            f.write(';'.join(_myfields) + "\n")

def _writeoutput (info=None, file=None):
    for variant in info:
        with open(file, 'a+') as f:
            f.write(str(';'.join([variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation,''])))
            for sample in range(len(variant.samples)):
                f.write(str(';'.join([variant.samples[sample].Genotype, str(variant.samples[sample].Depth), str(variant.samples[sample].Ratio), str(variant.samples[sample].SbRef), str(variant.samples[sample].SbVar),''])))
            f.write(str(';'.join([variant.Gene, variant.Feature, variant.Consequence, variant.cDNA_position, variant.Codons, variant.Protein_position, variant.Amino_acids, variant.CLIN_SIG, variant.SIFT, variant.PUBMED, variant.GMAF])) + '\n')

def _main_(argv):
    if not argv:
        print 'merging_var_anno.py -v <vcf file> -o <output file>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hv:o:")

    except getopt.GetoptError:
        print 'merging_var_anno.py -v <vcf file> -o <output file>'
        sys.exit(2)

    vcf_file, output_file= '', ''
    for opt, arg in opts:
        if opt == '-h':
            print 'merging_var_anno.py -v <vcf file> -o <output file>'
            sys.exit()
        elif opt == '-v':
            vcf_file = arg
        elif opt == '-o':
            output_file = arg
    if vcf_file == '':
        print "<<ERROR>> not input file"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _myheader,_myvariants = _getvariants(file=vcf_file)
    _printheader (header = _myheader, file=output_file)
    _writeoutput (info=_myvariants, file=output_file)
    _printsummary(info=_myvariants, file=output_file)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
