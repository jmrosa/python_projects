#!/usr/bin/env python
"""
SYNOPSIS
    ensemblVEP.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    ensemblVEP.py v0.1
"""

__author__ = 'jmrosa'

import sys
import os
import getopt
import re
from tools import *

def _launch_VEP_(file=None, output=None, threads=None, version=None):
    # _command = ''
    _output = output  + '.VEP'
        number = "98"
	dirs = ["hg19"="98_GRCh37", "hg38"="98_GRCh38"]
        _command = "/home/jmrosa/tools/ensembl/vep -i %s --cache --cache_version %s --dir_cache /home/jmrosa/.vep/homo_sapiens/%s --species homo_sapiens --fork 32 --everything --vcf --buffer_size 500000 --offline --fasta /home/jmrosa/genomes/%s/%s.fa --force_overwrite -o %s" %(file, number, dirs[ver], _output, ver, ver)
    result = _command_(command=_command)
    if result != 0:
        print "Error launching command for command %s" %(_command)
        sys.exit(1)

    #     os.remove(file)
    #     os.remove(file + '.VEP_summary.html')
    return _output

def _launch_MVA_(file=None, sample_file=None, anexes=None, threads=None, output=None, version=None):
   _command = "/usr/bin/python /home/jmrosa/git/python_projects/scripts/annotation/merging_var_anno.py -f %s -s %s -v %s %s %s %s -o %s 2> %s" %(file, sample_file, version, anexes[0], anexes[1], anexes[2], output, output + '.log')
   _command = _command.replace('   ', '')
   print _command
   result = _command_(command = _command)
   if result != 0:
       print result
       print "Error launching command for command %s" %(_command)
       sys.exit(1)
    #     os.remove(file)

def _getanexes_(can = None, fpos = None, pat = None):
    _anexes = []
    if can != ' ': can = "-c %s" %can
    if fpos != ' ': fpos = "-k %s" %fpos
    if pat != ' ': pat = "-p %s" %pat
    _anexes.append(can)
    _anexes.append(fpos)
    _anexes.append(pat)
    return _anexes
def _main_(argv):
	help='ensemblVEP.py -i <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -p <pathogenic variants file> -v <genome version:hg19 or hg38, default hg19> -o <output file> -t <threads>'

    if not argv:
        print help
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hi:s:c:k:o:p:t:v:")

    except getopt.GetoptError:
        print help
        sys.exit(2)

    vcf_file, sam_file, can_file, fpos_file, output_file, pat_file, threads, version = '', '',' ',' ','',' ', 1,'hg19'
    for opt, arg in opts:
        if opt == '-h':
            print help
            sys.exit()
        elif opt == '-i':
            vcf_file = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '-c':
            can_file = arg
        elif opt == '-k':
            fpos_file = arg
        elif opt == '-o':
            output_file = arg
        elif opt == '-t':
            threads = arg
        elif opt == '-p':
            pat_file = arg
        elif opt == '-v':
            version = arg
	    if version == 'cDNA': version = 'hg38'

    if vcf_file == '' or sam_file == '':
        print "<<ensemblVEP.py ERROR>>"
        print "<<ERROR>> not input files"
        sys.exit(1)

    if version != 'hg19' and version != 'hg38':
        print "<<ERROR>> invalid genome version:%s" %version
        print "Valid versions: hg19 or hg38 or cDNA"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _annofile = _launch_VEP_(file=vcf_file, output=output_file, threads = threads, version=version)
    _myanexes = _getanexes_(can = can_file, fpos = fpos_file, pat = fpos_file)
    _launch_MVA_(file = _annofile, sample_file = sam_file, anexes = _myanexes, threads = threads, output = output_file, version = version)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
