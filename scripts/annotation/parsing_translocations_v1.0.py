#!/usr/bin/python
"""
SYNOPSIS
    parsing_translocations_v1.0.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    parsing_translocations.py v0.1
"""
__author__ = 'jmrosa'

import sys
import re
import getopt
import os
import pprint

from tools import _gettime_
from pysam import VariantFile

def _getsamples(file=None):
    _mysamples = []
    with open(str(file), "r") as f:
        for line in f:
    # line.rstrip("\r\n")
            line = line[:-1]
            _mysamples.append(line.split(','))
    return _mysamples

def _printsummary(info=None, file=None, samples=None):
    _path = '/'.join(file.split('/')[:-1])
    for n in range(0, len(samples)):
        _output = _path + '/' + "_".join(samples[n]) + '_filtered.txt'
        with open(_output, 'w') as f:
            _myfields = ["#Symbol","Chr","Position","RefAllele","VarAllele","ID","GMAF"]
            for sample in samples[n]:
                _myfields.extend([sample + "_genotype",sample + "_depth",sample + "_ratio",sample + "_sbref",sample + "_sbvar"])
                _myfields.extend(["Gene","Transcript_MANE_Select","Consequence","cDNA_pos","Prot_pos","Motif_name","Motif_pos","Motif_score","Clinical","Sift","Polyphen","EU_MAF","AM_MAF","AF_MAF","Pubmed"])
            f.write(','.join(_myfields) + "\n")
            for variant in info:
                if re.search('/', variant.Amino_acids) != None: variant.Change = ''.join(["p.", variant.Amino_acids.split("/")[0] , variant.Protein_position, variant.Amino_acids.split("/")[1]])
                if re.search(':', variant.HGVSc) != None: variant.cDNA = str(variant.HGVSc).split(":")[1]
                _mysamples = []
                for sample in samples[n]:
                    for index in range(len(variant.samples)):
                        if variant.samples[index].name == sample:
                            _mysamples.extend([variant.samples[index].Genotype, str(variant.samples[index].Depth), str(variant.samples[index].Ratio), str(variant.samples[index].SbRef), str(variant.samples[index].SbVar)])
                _mysamplesdata=",".join(_mysamples)
                f.write(','.join([variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation, variant.AF, _mysamplesdata, variant.SYMBOL, variant.Canonic, variant.Consequence, variant.cDNA, variant.Change, variant.MOTIF_NAME, variant.MOTIF_POS, variant.MOTIF_SCORE_CHANGE, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.EUR_AF, variant.AMR_AF, variant.AFR_AF, variant.PUBMED]) + "\n")

test2print= 'parsing_translocations_v1.0.py -f <bcf file> -s <samples file> -o <output file>'
 
def _main_(argv):
    if not argv:
        print test2print
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hf:o:s:", ['vaf='])

    except getopt.GetoptError:
        print test2print
        sys.exit(2)
    global bcf_file, output_file, sam_file, vaf
    bcf_file, output_file, sam_file, vaf = None, None, None, 0.01
    for opt, arg in opts:
        if opt == '-h':
            print test2print
            sys.exit()
        elif opt == '-f':
            bcf_file = arg
        elif opt == '-o':
            output_file = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '--vaf':
            vaf = arg
    if bcf_file == None or sam_file == None:
        print "<<parsing_translocations_v1.0.py ERROR>>"
        print "<<ERROR>> not input files"
        sys.exit(1)

    sys.stderr.write("Process starts... " + _gettime_() + "\n")
    bcf_in = VariantFile(bcf_file)
    print "chr pos ref var chr2 end pe consensus sample rc rcl rcr cn dr dv rr rv ratio_dv ratio_dr sample"
    for rec in bcf_in.fetch():
         if rec.info["SVTYPE"] == "BND":
            if "CONSENSUS" not in rec.info.keys(): rec.info["CONSENSUS"] = "IMPRECISE"
            rec.stop = int(str(rec.alts).split(":")[1].split("]")[0].split("[")[0])
            n_samples = 0
            for sample in rec.samples.keys(): 
                if (float(float(rec.samples[sample]["RV"])/(float(rec.samples[sample]["RR"]) + float(rec.samples[sample]["RV"]) + 1)) > 0.0 or float(float(rec.samples[sample]["DV"])/(float(rec.samples[sample]["DR"]) + float(rec.samples[sample]["DV"]) + 1)) > 0.0) and rec.samples[sample]["DV"] > 2 and rec.samples[sample]["DV"] + rec.samples[sample]["DR"] > 20: n_samples += 1
            for sample in rec.samples.keys(): 
                if (float(float(rec.samples[sample]["RV"])/(float(rec.samples[sample]["RR"]) + float(rec.samples[sample]["RV"]) + 1)) > 0.0 or float(float(rec.samples[sample]["DV"])/(float(rec.samples[sample]["DR"]) + float(rec.samples[sample]["DV"]) + 1)) > 0.0) and rec.samples[sample]["DV"] > 2 and rec.samples[sample]["DV"] + rec.samples[sample]["DR"] > 20:
                    print rec.chrom,rec.pos,rec.ref,str(rec.alts[0]),rec.info["CHR2"],rec.stop,rec.info["PE"],rec.info["CONSENSUS"],sample,rec.samples[sample]["RC"],rec.samples[sample]["RCL"],rec.samples[sample]["RCR"],rec.samples[sample]["CN"],rec.samples[sample]["DR"],rec.samples[sample]["DV"],rec.samples[sample]["RR"],rec.samples[sample]["RV"],"{0:.3f}".format(float(float(rec.samples[sample]["DV"])/(float(rec.samples[sample]["DR"]) + float(rec.samples[sample]["DV"]) + 1))),"{0:.3f}".format(float(float(rec.samples[sample]["RV"])/(float(rec.samples[sample]["RR"]) + float(rec.samples[sample]["RV"]) + 1))),n_samples
        
        
    # _myvariants = _getvariants(file=vcf_file, VAF=vaf)
    # _mysamples = _getsamples(file=sam_file)
    # _printsummary(info=_myvariants, file=output_file, samples=_mysamples)
    # _writeoutput (info=_myvariants, file=output_file, version=version)
    sys.stderr.write ("Process finished... " + _gettime_() + "\n")
    sys.exit(0)

_main_(sys.argv[1:])
