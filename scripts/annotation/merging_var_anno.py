#!/usr/bin/env python
"""
SYNOPSIS
    merging_var_anno.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    merging_var_anno.py v0.1
"""
__author__ = 'jmrosa'

import sys
import re
import getopt
import os
from tools import _gettime_
from variants import _getvariants

def _printsummary(info=None, file=None, canonic=None, falses=None, samples=None, pathogenic=None, version=None):
    _path = '/'.join(file.split('/')[:-1])
    for n in range(0, len(samples)):
        _output = _path + '/' + "_".join(samples[n]) + '_filtered.txt'
        with open(_output, 'w') as f:
            _myfields = ["#Symbol","Chr","Position","RefAllele","VarAllele","ID"]
            for sample in samples[n]:
                _myfields.extend([sample + "_genotype",sample + "_depth",sample + "_ratio",sample + "_sbref",sample + "_sbvar"])
            _myfields.extend(["Gene","Transcript","Consequence","GMAF","cDNA_pos","Prot_pos","AminoAcids","Motif_name","Motif_pos","Motif_score","Clinical","Sift","Polyphen","EU_MAF","AM_MAF","AF_MAF","Pubmed"])
            f.write(','.join(_myfields) + "\n")
            for variant in info:
                if canonic != None: _iscanonic = variant.isincanonic(can=canonic)
                else:
                    _iscanonic = 1
                    variant.Canonic = variant.CANONICAL
                if re.search('/', variant.Amino_acids) != None: variant.Change = ''.join([variant.Amino_acids.split("/")[0] , variant.Protein_position, variant.Amino_acids.split("/")[1]])
                if pathogenic != None and re.search('missense', variant.Consequence) != None: variant.ismissense(file=pathogenic)
                _isdeleterious = variant.isdeleterious()
                if falses != None: _isfalsepositive = variant.isfalsepositive(file=falses)
                else: _isfalsepositive = 0
                if _isdeleterious == 1 and _iscanonic == 1 and _isfalsepositive == 0:
                    _mysamples = []
                    _mynumber = 0
                    for sample in samples[n]:
                        for index in range(len(variant.samples)):
                            if variant.samples[index].name == sample:
                                _mysamples.extend([variant.samples[index].Genotype, str(variant.samples[index].Depth), str(variant.samples[index].Ratio), str(variant.samples[index].SbRef), str(variant.samples[index].SbVar)])
                                if variant.samples[index].Ratio != '-' and variant.samples[index].Ratio >= 0.10:
                                    _mynumber += 1
                                break

                    if _mynumber > 0:
                        _mysamplesdata=",".join(_mysamples)
                        if version == "hg20":
                            f.write(','.join([variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation, _mysamplesdata, variant.Gene, variant.Canonic, variant.Consequence, variant.AF, variant.HGVSc,variant.HGVSp, variant.Change, variant.MOTIF_NAME, variant.MOTIF_POS, variant.MOTIF_SCORE_CHANGE, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.EUR_AF, variant.AMR_AF, variant.AFR_AF, variant.PUBMED]) + "\n")
                        elif version == "hg19":
                            f.write(','.join([variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation, _mysamplesdata, variant.Gene, variant.Canonic, variant.Consequence, variant.GMAF, variant.HGVSc,variant.HGVSp, variant.Change, variant.MOTIF_NAME, variant.MOTIF_POS, variant.MOTIF_SCORE_CHANGE, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.EUR_MAF, variant.AMR_MAF, variant.AFR_MAF, variant.PUBMED]) + "\n")

def _getsamples(file=None):
    _mysamples = []
    with open(str(file), "r") as f:
        for line in f:
            # line.rstrip("\r\n")
            line = line[:-1]
            _mysamples.append(line.split(','))
    return _mysamples

def _printheader (header=None, file=None):
        _path = '/'.join(file.split('/')[:-1])
        if not os.path.exists(_path):
            os.makedirs(_path)
        _myfields = ["#Symbol","Chr","Position","RefAllele","VarAllele","ID"]
        for sample in header.split('\t')[9:]:
            _myfields.extend([sample + "_genotype",sample + "_depth",sample + "_ratio",sample + "_sbref",sample + "_sbvar"])
        _myfields.extend(["Gene","Transcript","Canonic","Consequence","cDNA_pos","Prot_pos","AminoAcids","Motif_name","Motif_pos","Motif_score","Clinical","Sift","Polyphen","EU_MAF","AM_MAF","AF_MAF","GMAF","Pubmed"])
        with open(file, 'w') as f:
            f.write(','.join(_myfields) + "\n")

def _writeoutput (info=None, file=None, version=None):
    for variant in info:
        with open(file, 'a+') as f:
            f.write(str(','.join([variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation,''])))
            for sample in range(len(variant.samples)):
                f.write(str(','.join([variant.samples[sample].Genotype, str(variant.samples[sample].Depth), str(variant.samples[sample].Ratio), str(variant.samples[sample].SbRef), str(variant.samples[sample].SbVar),''])))
            if version == "hg20":
                f.write(str(','.join([variant.Gene, variant.Feature, str(variant.Canonic), variant.Consequence, variant.HGVSc, variant.HGVSp, variant.Change, variant.MOTIF_NAME, variant.MOTIF_POS, variant.MOTIF_SCORE_CHANGE, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.EUR_AF, variant.AMR_AF, variant.AFR_AF, variant.AF, variant.PUBMED])) + '\n')
            elif version == "hg19":
                f.write(str(','.join([variant.Gene, variant.Feature, str(variant.Canonic), variant.Consequence, variant.HGVSc, variant.HGVSp, variant.Change, variant.MOTIF_NAME, variant.MOTIF_POS, variant.MOTIF_SCORE_CHANGE, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.EUR_MAF, variant.AMR_MAF, variant.AFR_MAF, variant.GMAF, variant.PUBMED])) + '\n')


def _main_(argv):
    if not argv:
        print 'merging_var_anno.py -f <vcf file> -s <samples file> -c <canonic file> -k <known variants files> -p <pathogenic variants file> -o <output file> -v <hg19 or hg20, optional, default hg20>>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hf:o:c:s:k:p:v:")

    except getopt.GetoptError:
        print 'merging_var_anno.py -f <vcf file> -s <samples file> -c <canonic file> -k <known variants files> -p <pathogenic variants file> -o <output file> -v <hg19 or hg20, optional, default hg20>>'
        sys.exit(2)

    vcf_file, output_file, sam_file, can_file, var_file, pat_file, version= None, None, None, None, None, None, "hg20"
    for opt, arg in opts:
        if opt == '-h':
            print 'merging_var_anno.py -f <vcf file> -s <samples file> -c <canonic file> -k <known variants files> -p <pathogenic variants file> -o <output file> -v <hg19 or hg20, optional, default hg20>'
            sys.exit()
        elif opt == '-f':
            vcf_file = arg
        elif opt == '-o':
            output_file = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '-c':
            can_file = arg
        elif opt == '-k':
            var_file = arg
        elif opt == '-p':
            pat_file = arg
        elif opt == '-v':
            version = arg

    if vcf_file == None or sam_file == None:
        print "<<merging_var_anno.py ERROR>>"
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _myheader,_myvariants = _getvariants(file=vcf_file)
    _printheader (header = _myheader, file=output_file)
    _mysamples = _getsamples(file=sam_file)
    _printsummary(info=_myvariants, file=output_file, canonic=can_file, falses=var_file, samples=_mysamples, pathogenic=pat_file, version=version)
    _writeoutput (info=_myvariants, file=output_file, version=version)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
