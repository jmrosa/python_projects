#!/usr/bin/python
"""
SYNOPSIS
    merging_var_anno_v2.0.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    merging_var_anno.py v0.1
"""
__author__ = 'jmrosa'

import sys
import re
import getopt
import os

from tools import _gettime_
from variants_v2_0 import _getvariants

def _getsamples(file=None):
    _mysamples = []
    with open(str(file), "r") as f:
        for line in f:
    # line.rstrip("\r\n")
            line = line[:-1]
            _mysamples.append(line.split(','))
    return _mysamples

def getfieldsheader(info=None, type=None):
    _myfields = ["#Symbol","Chr","Position","RefAllele","VarAllele","ID","GMAF"]
    for sample in info:
        _myfields.extend([sample + "_genotype",sample + "_depth",sample + "_ratio",sample + "_sbref",sample + "_sbvar"])
    if abl1 == 'YES':
        _myfields.extend(["Transcript_MANE_Select","Consequence","cDNA_pos","Prot_pos","Clinical","Sift","Polyphen","Pubmed"])
    else:
        _myfields.extend(["Transcript_MANE_Select","Consequence","cDNA_pos","Prot_pos","Motif_name","Motif_pos","Motif_score","Clinical","Sift","Polyphen","EU_MAF","AM_MAF","AF_MAF","Pubmed"])
    return _myfields

def getfieldsvar(info=None, type=None, variant=None):
    _myfields = [variant.SYMBOL, variant.chr, variant.pos, variant.ref, variant.var, variant.Existing_variation, variant.AF]
    for sample in info:
        for index in range(len(variant.samples)):
            if variant.samples[index].name == sample:
               _myfields.extend([variant.samples[index].Genotype, str(variant.samples[index].Depth), str(variant.samples[index].Ratio), str(variant.samples[index].SbRef), str(variant.samples[index].SbVar)])
    if abl1 == 'YES':
        _myfields.extend([variant.Canonic, variant.Consequence, variant.cDNA, variant.Change, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.PUBMED])
    else:
        _myfields.extend([variant.Canonic, variant.Consequence, variant.cDNA, variant.Change, variant.MOTIF_NAME, variant.MOTIF_POS, variant.MOTIF_SCORE_CHANGE, variant.CLIN_SIG, variant.SIFT, variant.PolyPhen, variant.EUR_AF, variant.AMR_AF, variant.AFR_AF, variant.PUBMED])
    return _myfields

def check_ratios(variant = None, samples = None):
    result = "FAILED"
    for sample in samples:
        for index in range(len(variant.samples)):
            if variant.samples[index].name == sample:
                if variant.samples[index].Ratio > vaf:
                    result = "PASSED" 
    return result

def check_consequence(variant = None):
    result = "PASSED"
    if variant.Consequence == 'synonymous_variant' or re.search('prime_UTR', variant.Consequence) != None or re.search('intron_variant', variant.Consequence) != None or variant.Consequence == 'upstream_gene_variant' or variant.Consequence == 'downstream_gene_variant': result = "FAILED"
    return result

def check_variant(variant = None, samples = None):
    result = "PASSED"
    if check_ratios(variant = variant, samples = samples) == "FAILED": result = "FAILED"
    if check_consequence(variant) == "FAILED": result = "FAILED"
    if (variant.AF != '' and variant.AF > 0.01) or (variant.EUR_AF != '' and variant.EUR_AF > 0.01): result = "FAILED"
    if abl1 == "YES":
        if variant.Consequence == 'frameshift_variant' or (variant.HGVSc != '' and (re.search('\+', variant.HGVSc) != None or re.search('\-', variant.HGVSc) != None)): result = "FAILED"
    return result

def _printsummary(info=None, file=None, samples=None):
    _path = '/'.join(file.split('/')[:-1])
    for n in range(0, len(samples)):
        _output = _path + '/' + "_".join(samples[n]) + '_' + str(vaf) + '_filtered.txt'
        with open(_output, 'w') as f:
            f.write(','.join(getfieldsheader(info=samples[n], type = abl1)) + "\n")
            for variant in info:
                if check_variant(variant = variant, samples = samples[n]) == "FAILED": continue
                if re.search('/', variant.Amino_acids) != None: variant.Change = ''.join(["p.", variant.Amino_acids.split("/")[0] , variant.Protein_position, variant.Amino_acids.split("/")[1]])
                if re.search(':', variant.HGVSc) != None: variant.cDNA = str(variant.HGVSc).split(":")[1]
                f.write(','.join(getfieldsvar(info=samples[n], variant = variant, type = abl1)) + "\n")

test2print= 'merging_var_anno_v2.0.py -f <vcf file> -s <samples file> -o <output dir> --vaf <VAF threshold, default = 0.01>'
 
def _main_(argv):
    if not argv:
        print test2print
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hf:o:s:", ['vaf=', 'ABL1'])

    except getopt.GetoptError:
        print test2print
        sys.exit(2)
    global vcf_file, output_file, sam_file, vaf, abl1
    vcf_file, outdir, sam_file, vaf, abl1 = None, "./", None, 0.01, None
    for opt, arg in opts:
        if opt == '-h':
            print test2print
            sys.exit()
        elif opt == '-f':
            vcf_file = arg
        elif opt == '-o':
            outdir = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '--vaf':
            vaf = float(arg)
        elif opt == '--ABL1':
            abl1 = "YES"
    if vcf_file == None or sam_file == None:
        print "<<merging_var_anno_v2.0.py ERROR>>"
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _myvariants = _getvariants(file=vcf_file, VAF=vaf)
    _mysamples = _getsamples(file=sam_file)
    _printsummary(info=_myvariants, file=outdir, samples=_mysamples)
    # _writeoutput (info=_myvariants, file=output_file, version=version)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
