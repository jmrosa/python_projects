#!/usr/bin/env python
"""
SYNOPSIS
    ensemblVEP.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    ensemblVEP.py v0.1
"""

__author__ = 'jmrosa'

import sys
import os
import getopt
import re
from tools import *

def _launch_VEP_(file=None, output=None, threads=None, version=None):
    # _command = ''
    _output = output  + '.VEP'

    if version == 'hg20':
        numbers = ['94','94']
        _command = "/input/jmrosa/git/ensembl-vep-%s/vep -i %s --cache --cache_version %s --species homo_sapiens --fork 32 --everything --vcf --buffer_size 500000 --offline --fasta /input/jmrosa/genomes/hg20/Homo_sapiens.GRCh38.dna.primary_assembly.fa --force_overwrite -o %s" %(numbers[0], file, numbers[1], _output)
    else:
        numbers = ['74','74']
        _command = "perl /opt/ensembl-tools-release-%s/scripts/variant_effect_predictor/variant_effect_predictor.pl -i %s --cache --cache_version %s --species homo_sapiens --fork 32 --everything --vcf --buffer_size 500000 --offline --fasta /input/jmrosa/genomes/hg19/hg19.chrM_fixed.fasta --force_overwrite -o %s" %(numbers[0], file, numbers[1], _output)
    result = _command_(command=_command)
    if result != 0:
        print "Error launching command for command %s" %(_command)
        sys.exit(1)

    #     os.remove(file)
    #     os.remove(file + '.VEP_summary.html')
    return _output

def _launch_MVA_(file=None, sample_file=None, anexes=None, threads=None, output=None, version=None):
   _command = "/usr/bin/python /input/jmrosa/git/python_projects/scripts/annotation/merging_var_anno.py -f %s -s %s -v %s %s %s %s -o %s 2> %s" %(file, sample_file, version, anexes[0], anexes[1], anexes[2], output, output + '.log')
   _command = _command.replace('   ', '')
   print _command
   result = _command_(command = _command)
   if result != 0:
       print result
       print "Error launching command for command %s" %(_command)
       sys.exit(1)
    #     os.remove(file)

def _getanexes_(can = None, fpos = None, pat = None):
    _anexes = []
    if can != ' ': can = "-c %s" %can
    if fpos != ' ': fpos = "-k %s" %fpos
    if pat != ' ': pat = "-p %s" %pat
    _anexes.append(can)
    _anexes.append(fpos)
    _anexes.append(pat)
    return _anexes
def _main_(argv):
    if not argv:
        print 'ensemblVEP.py -i <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -p <pathogenic variants file> -v <genome version:hg19 or hg20, default hg19> -o <output file> -t <threads>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hi:s:c:k:o:p:t:v:")

    except getopt.GetoptError:
        print 'ensemblVEP.py -i <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -p <pathogenic variants file> -v <genome version:hg19 or hg20, default hg19> -o <output file> -t <threads>'
        sys.exit(2)

    vcf_file, sam_file, can_file, fpos_file, output_file, pat_file, threads, version = '', '',' ',' ','',' ', 1,'hg19'
    for opt, arg in opts:
        if opt == '-h':
            print 'ensemblVEP.py -i <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -p <pathogenic variants file> -v <genome version:hg19 or hg20, default hg19> -o <output file> -t <threads>'
            sys.exit()
        elif opt == '-i':
            vcf_file = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '-c':
            can_file = arg
        elif opt == '-k':
            fpos_file = arg
        elif opt == '-o':
            output_file = arg
        elif opt == '-t':
            threads = arg
        elif opt == '-p':
            pat_file = arg
        elif opt == '-v':
            version = arg
	    if version == 'cDNA': version = 'hg20'

    if vcf_file == '' or sam_file == '':
        print "<<ensemblVEP.py ERROR>>"
        print "<<ERROR>> not input files"
        sys.exit(1)

    if version != 'hg19' and version != 'hg20':
        print "<<ERROR>> invalid genome version:%s" %version
        print "Valid versions: hg19 or hg20 or cDNA"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _annofile = _launch_VEP_(file=vcf_file, output=output_file, threads = threads, version=version)
    _myanexes = _getanexes_(can = can_file, fpos = fpos_file, pat = fpos_file)
    _launch_MVA_(file = _annofile, sample_file = sam_file, anexes = _myanexes, threads = threads, output = output_file, version = version)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
