#!/usr/bin/env python
"""
SYNOPSIS
    ensemblVEP.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    ensemblVEP.py v0.1
"""
__author__ = 'jmrosa'

import sys
import os
import getopt
import re
from tools import *

def _getfiles(file=None, output=None):
    _output = []
    _lines = 1
    _header = []
    _number = 0
    with open(file, 'r') as f:
        for line in f:
            line = line[:-1]
            if re.match('^#', line) != None: _header.append(line)
            else:
                _lines += 1
                if _lines >= _number * 100000:
                    _number += 1
                    _outfile = '.'.join(output.split('.')[:-1]) + '.tmp.' + str(_number)
                    _output.append(_outfile)
                    with open(_outfile, 'w') as o:
                        for n in range(len(_header)):
                            o.write(_header[n] + '\n')
                        o.write(line + '\n')
                else:
                    _outfile = '.'.join(output.split('.')[:-1]) + '.tmp.' + str(_number)
                    with open(_outfile, 'a+') as o:
                        o.write(line + '\n')

    return _output

def _launch_VEP_(files=None, threads=None):
    _commands = []
    _outputs = []

    for file in files:
        output = file + '.VEP'
        _outputs.append(output)
        _commands.append("perl /share/tools/variant_effect_predictor/variant_effect_predictor.pl -i %s --cache  --everything --vcf --force_overwrite -o %s" %(file, output))

    results = _parallel_(commands=_commands, threads=threads)
    for n in range(len(results)):
        if results[n] != 0:
            print "Error launching command for command %s" %(_commands[n])
            print "Error launching command for command %s" %{_commands[n]}
            sys.exit(1)
    for file in files:
        os.remove(file)
        os.remove(file + '.VEP_summary.html')
    return _outputs

def _launch_MVA_(files=None, anexes=None, threads=None):
    _commands = []
    _outputs = []

    for file in files:
        output = file + '.txt'
        _outputs.append(output)
        _commands.append("/usr/bin/python /home/jmrosa/git/python_projects/scripts/annotation/merging_var_anno.py -v %s -s %s -c %s -k %s -o %s 2> %s" %(file, anexes[0], anexes[1], anexes[2], output, output + '.log'))

    results = _parallel_(commands=_commands, threads=threads)
    for n in range(len(results)):
       if results[n] != 0:
           print results[n]
           print "Error launching command for command %s" %(_commands[n])
           sys.exit(1)
    for file in files:
        os.remove(file)
    return _outputs

def _writeoutput (output=None, files=None):
    with open(output, 'w') as f:
        for n in range(len(files)):
            with open(files[n], 'r') as i:
                for line in i:
                    line = line[:-1]
                    if re.match('^#', line) != None and n == 0: f.write(line + '\n')
                    elif re.match('^#', line) == None: f.write(line + '\n')
            os.remove(files[n])

def _main_(argv):
    if not argv:
        print 'ensemblVEP.py -v <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -o <output file> -t <threads>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hv:s:c:k:o:t:")

    except getopt.GetoptError:
        print 'ensemblVEP.py -v <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -o <output file> -t <threads>'
        sys.exit(2)

    vcf_file, sam_file, can_file, fpos_file, output_file, threads= '', '', '', '', '', 1
    for opt, arg in opts:
        if opt == '-h':
            print 'ensemblVEP.py -v <vcf file> -s <sample file> -c <canonics file> -k <false positive file> -o <output file> -t <threads>'
            sys.exit()
        elif opt == '-v':
            vcf_file = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '-c':
            can_file = arg
        elif opt == '-k':
            fpos_file = arg
        elif opt == '-o':
            output_file = arg
        elif opt == '-t':
            threads = arg
    if vcf_file == '' or sam_file == '' or can_file == '' or fpos_file == '':
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    _files = _getfiles(file=vcf_file, output=output_file)
    _annofiles = _launch_VEP_(files=_files, threads = threads)
    _outputs = _launch_MVA_(files = _annofiles, anexes = [sam_file,can_file,fpos_file], threads = threads)
    _writeoutput (files=_outputs, output = output_file)
    print "Process finished... " + _gettime_()
    sys.exit(0)

_main_(sys.argv[1:])
