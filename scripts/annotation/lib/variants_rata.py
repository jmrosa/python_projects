#!/usr/bin/env python
"""
SYNOPSIS
    variants.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Module to analyse variant information
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    variants.py v0.1
"""
__author__ = 'jmrosa'

import re


def _getvariants(file=None):
    _myvariants = []
    _header, _info = '',''
    with open(str(file), "r") as f:
        for line in f:
            line = line[:-1]
            if re.match('#CHROM', line) != None: _header = line
            elif re.match('##INFO=<ID=CSQ', line) != None: _info = line
            elif re.match('^chr', line) != None:
                _myfields = line.split('\t')
                for _trans in re.split('CSQ=|,', _myfields[7])[1:]:
                    _variant = Variant(chr=_myfields[0], pos=_myfields[1], ref=_myfields[3], var=_myfields[4])
                    _variant._getsamples(header=_header, line=line)
                    _variant._getinfo(info=_info, line=_trans)
                    _myvariants.append(_variant)
    return _header, _myvariants

# Class definitions
class Variant (object):
    """Class essay to create variants objects"""
    def __init__(self, chr=None, pos=None, ref=None, var=None):
        self.chr = chr
        self.pos = pos
        self.ref = ref
        self.var = var
        self.samples = []

    def _getsamples(self, header=None, line=None):
        _myheader = header.split('\t')
        _myinfo = line.split('\t')
        for sample in range(9, len(_myinfo)):
            self.samples.append(Sample(name=_myheader[sample]))
            self.samples[-1]._updateinfo(_myinfo[sample])

    def _getinfo(self, info=None, line=None):
        _myinfo = {}
        info = info.replace('">', '')
        for field in range(0, len(info.split('Format: ')[1].split('|'))):
            _myinfo[info.split('Format: ')[1].split('|')[field]] = re.split('CSQ=|\|', line)[field]
        self.__dict__.update(_myinfo)

    def isdeleterious(self):
        if re.search('frameshift', self.Consequence) != None or re.search('inframe', self.Consequence) != None or re.search('stop', self.Consequence) != None or re.search('splice', self.Consequence) != None: return 1
        elif self.CLIN_SIG != '' and self.CLIN_SIG != 'non-pathogenic': return 1
        elif re.search('missense', self.Consequence) != None and re.search('tolerated', self.SIFT): return 1
        else: return 0

class Sample (object):
    """Class essay to create variants objects"""
    def __init__(self, name=None):
        self.name = name

    def _updateinfo(self, info=None):
        _mydata = info.split(':')
        _myinfo = {"Depth":'-',"Ratio":'-',"SbRef":'-',"SbVar":'-',"Genotype":'-'}
        if _mydata[0] != './.':
            _myinfo["Depth"] = int(_mydata[3])
            if (int(_mydata[5]) > 0 and int(_mydata[3]) > 0):
                _myratio = float(_mydata[5])/int(_mydata[3])
                _myinfo["Genotype"] = self._getgenotype(_myratio)
                _myinfo["Ratio"] = _myratio
            elif (int(_mydata[5]) > 0 and int(_mydata[3]) == 0):
                _myinfo["Ratio"] = 1
                _myinfo["Genotype"] = self._getgenotype(_myinfo["Ratio"])
            elif (int(_mydata[5]) == 0 and int(_mydata[3]) > 0):
                _myinfo["Ratio"] = 0
                _myinfo["Genotype"] = self._getgenotype(_myinfo["Ratio"])
            if (int(_mydata[11]) > 0 and int(_mydata[10]) > 0):
                _myinfo["SbRef"] = "{0:.2f}".format(float(_mydata[10])/(int(_mydata[10]) + int(_mydata[11])))
            elif (int(_mydata[11]) > 0 and int(_mydata[10]) == 0):
                _myinfo["SbRef"] = '---'
            elif (int(_mydata[11]) == 0 and int(_mydata[10]) > 0):
                _myinfo["SbRef"] = '+++'
            if (int(_mydata[13]) > 0 and int(_mydata[12]) > 0):
                _myinfo["SbVar"] = "{0:.2f}".format(float(_mydata[12])/(int(_mydata[12]) + int(_mydata[13])))
            elif (int(_mydata[13]) > 0 and int(_mydata[12]) == 0):
                _myinfo["SbVar"] = '---'
            elif (int(_mydata[13]) == 0 and int(_mydata[12]) > 0):
                _myinfo["SbVar"] = '+++'
        self.__dict__.update(_myinfo)

    def _getgenotype(self, ratio=None):
        if ratio == 0: return "Homo_ref"
        elif ratio > 0 and ratio < 0.15: return "P_Homo_ref"
        elif ratio >= 0.15 and ratio < 0.35: return "Unc_Hetero"
        elif ratio >= 0.35 and ratio < 0.65: return "P_Hetero"
        elif ratio >= 0.65 and ratio < 0.85: return "Unc_Homo_var"
        elif ratio >= 0.85: return "P_Homo_var"
        else: return "Unknown"


