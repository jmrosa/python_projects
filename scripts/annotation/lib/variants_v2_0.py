#!/usr/bin/env python
"""
SYNOPSIS
    variants_v2.0.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Module to analyse variant information
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    variants_v2.0.py v0.1
"""
__author__ = 'jmrosa'

import re
import vcf
import gzip
import yaml
from pprint import pprint


def _getvariants(file=None, VAF = None):
    _myvariants = []
    _header = Header(name="Header")
    vcf_reader = vcf.Reader(filename = file)
    _header.create(info=vcf_reader.infos["CSQ"])
    for record in vcf_reader:
        alleles = []
        for csq in record.INFO["CSQ"]:
            if csq.split('|')[_header.MANE_SELECT] != "":
                allele = csq.split('|')[_header.Allele]
                al_order = ''
                if allele in alleles: al_order = alleles.index(allele)
                else: 
                    alleles.append(allele)
                    al_order = alleles.index(allele)
                var = str(record.ALT).split(",")[al_order].replace("[", "").replace("]", "").replace(" ", "")
                _variant = Variant(chr=record.CHROM, pos=record.POS, ref=record.REF, var=var, ord=al_order, vaf=VAF, all=allele, can = csq.split('|')[_header.MANE_SELECT])  
                _variant._getsamples(data=record)
                if _variant.Pass == 'Passed':
                    _variant._getinfo(info=csq, head=_header)
                    _myvariants.append(_variant)                      
                    #pprint (vars(_variant))
    return _myvariants

# Class definitions
class Variant (object):
    """Class essay to create variants objects"""
    def __init__(self, chr=None, pos=None, ref=None, var=None, ord = None, vaf = None, all = None, can =None):
        self.chr = chr
        self.pos = str(pos)
        self.ref = ref
        self.var = var
        self.samples = []
        self.Canonic = can
        self.Change = '-'
        self.allele = all
        self.Pass = None
        self.VAF = vaf
        self.al_ord = ord + 1
        self.cDNA = '-'

    def _getsamples(self, data=None):
        for sample in data.samples:
            self.samples.append(Sample(name=sample.sample))
            self.samples[-1]._updateinfo(info=sample, var = self)
            if self.samples[-1].Pass == "Passed":
                self.Pass="Passed"

    def _getinfo(self, info=None, head=None):
        _myinfo = {}
        data = str(info).split('|')
        for attr, value in head.__dict__.iteritems():
            if attr != "name":
                #print "my attr. %s in pos: %s" %(attr, int(value))
                _myinfo[attr] = data[int(value)]
        self.__dict__.update(_myinfo)

    def ismissense(self, file=None):
        with open(str(file), "r") as f:
            for line in f:
                line = line[:-2]
                if re.search(str(self.CANONICAL), line) != None:
                    _myfields = line.split(',')
                    if self.Change == _myfields[1]:
                        self.CLIN_SIG = 'pathogenic'
                        break

    def isdeleterious(self):
        if re.search('frameshift', self.Consequence) != None or re.search('inframe', self.Consequence) != None or re.search('stop', self.Consequence) != None or re.search('acceptor', self.Consequence) != None or re.search('donor', self.Consequence) != None or (re.search('splice', self.Consequence) != None and re.search('synonymous', self.Consequence) != None): return 1
        elif re.search('missense', self.Consequence)!= None and (re.search('tolerated', self.SIFT) == None or re.search('benign', self.PolyPhen) == None): return 1
        elif self.CLIN_SIG != '' and self.CLIN_SIG != 'non-pathogenic': return 1
        else: return 0

    def isfalsepositive(self, file=None):
        _myfields= str(self.chr + ":" + self.pos + "-" + self.ref + "_" + self.var)
        with open(str(file), "r") as f:
            for line in f:
                line = line[:-1]
                _mydata = line.split(',')[0]
                if _myfields == _mydata:
                    #print "-- False Positive"
                    return 1
        return 0

    def isincanonic(self, can=None):
        (_mycanonic, _enstranscript) = self.getcanonic(source=can)
        if _mycanonic != None:
            if _enstranscript == str(self.Feature):
                self.Canonic = _mycanonic
                return 1
            else: return 0
        else: return 0

    def getcanonic(self, source=None):
        with open(str(source), "r") as f:
            for line in f:
                line = line[:-2]
                if re.search(str(self.SYMBOL), line) != None:
                    _myfields = line.split(',')
                    return _myfields[3], _myfields[2]
        return None, None


class Sample (object):
    """Class essay to create variants objects"""
    def __init__(self, name=None):
        self.name = name
        self.Pass = None

    def _updateinfo(self, info=None, var = None):
        _myinfo = {"Depth":'-',"Ratio":'-',"SbRef":'-',"SbVar":'-',"Genotype":'-'}
        # print "My depth is %s for var order: %s" %(info["AD"][var.al_ord], var.al_ord)
        if info["GT"] != './.':
            _myinfo["Depth"] = info["DP"]
            _myratio = float("{0:.4f}".format(float(info["AD"][var.al_ord])/int(info["DP"] + 1)))
            _myinfo["Genotype"] = self._getgenotype(_myratio)
            _myinfo["Ratio"] = _myratio
            #print "{0:.3f}".format(float(info["ADF"][0]) / float(info["ADR"][0] + info["ADF"][0] + 1))
            _myinfo["SbRef"] = "{0:.3f}".format(float(info["ADF"][0]) / float(info["ADR"][0] + info["ADF"][0] + 1))
            _myinfo["SbVar"] = "{0:.3f}".format(float(info["ADF"][var.al_ord]) / float(info["ADR"][var.al_ord] + info["ADF"][var.al_ord] + 1))
            # print  "My ratio: %s my VAF: %s" % (_myratio, float(var.VAF))
            if (_myratio > float(var.VAF)):
                _myinfo["Pass"] = "Passed"
        self.__dict__.update(_myinfo)

    def _getgenotype(self, ratio=None):
        if ratio == 0: return "Homo_ref"
        elif ratio > 0 and ratio < 0.15: return "P_Homo_ref"
        elif ratio >= 0.15 and ratio < 0.35: return "Unc_Hetero"
        elif ratio >= 0.35 and ratio < 0.65: return "P_Hetero"
        elif ratio >= 0.65 and ratio < 0.85: return "Unc_Homo_var"
        elif ratio >= 0.85: return "P_Homo_var"
        else: return "Unknown"

class Header (object):
    """Class essay to create header object"""
    def __init__(self, name=None):
        self.name = name
    
    def create(self, info=None):
        _mydata = {}
        _mydata["Allele"] = 0
        for element in str(info).split('|')[1::]:
            _mydata[element] = int(str(info).split('|').index(element))
        self.__dict__.update(_mydata)
