#!/usr/bin/python
"""
SYNOPSIS
    bcftools_filter_var_anno.py [-h,--help] [--ferbose] [--fersion]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    bcftools_filter_var_anno.py v1.0
"""
__author__ = 'jmrosa'

import sys
import re
import getopt
import os
import vcf
import gzip
from tools import _gettime_
from variants import _getvariants

def _filter_vcf(vcf_object = None):
    _name = str(str(vcf_file.split('/')[-1]).split('.')[0])
    _output = outdir + '/' + _name + "_filtered.vcf"
    vcf_writer = vcf.Writer(open(_output, "w"), vcf_object)
    for record in vcf_object:
        if str(record.ALT) != '[None]':
            filter = None
            for sample in record.samples:
                for n in range(1, len(record.ALT) + 1):
                    depth = float(sample["AD"][0]) + float(sample["AD"][n])
                    if depth == 0:
                        ratio = 0
                    else:
                        ratio = float(sample["AD"][n]) / float(depth)
                    if ratio > vaf:
                        if sample["AD"][n] > AD and depth > DP:
                            # print str(ratio) + " " + str(vaf) + " " + str(sample) + "\n"
                            filter = 'PASS'             
            if filter == 'PASS':
                vcf_writer.write_record(record)

test2print = 'bcftools_filter_var_anno.py -f <vcf file> -v <VAF to filter; default = 0.01> -D <depth to filter; default = 20> -d <Variant depth to filter; default = 2> -o <output dir path>'
def _main_(argv):
    if not argv:
        print test2print
        sys.exit(1)
    try:
        opts, args = getopt.getopt(argv, "hf:o:v:")

    except getopt.GetoptError:
        print test2print
        sys.exit(2)
    global vcf_file, outdir, vaf, DP, AD
    vcf_file, outdir, vaf, DP, AD = None, None, 0.01, 20, 2
    for opt, arg in opts:
        if opt == '-h':
            print test2print
            sys.exit()
        elif opt == '-f':
            vcf_file = arg
        elif opt == '-o':
            outdir = arg
        elif opt == '-v':
            vaf = float(arg)
        elif opt == '-d':
            DP = float(arg)
        elif opt == '-D':
            AD = float(arg)

    if vcf_file == None:
        print "<<bcftools_filter_var_anno.py ERROR>>"
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()
    vcf_reader = vcf.Reader(filename = vcf_file)
    vcf_filter = _filter_vcf(vcf_object=vcf_reader)
    print "Process finished... " + _gettime_()
    sys.exit(0)


_main_(sys.argv[1:])

