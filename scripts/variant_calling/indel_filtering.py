#!/usr/bin/env python
"""
SYNOPSIS
    start.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    ensemblVEP.py v0.1
"""

__author__ = 'jmrosa'

import getopt
import re
import sys
from datetime import datetime

import numpy as np


def _getsamples_(file=None):
    _samples= []
    with open(file, 'r') as f:
        for line in f:
            _samples.append(line[:-1])
    return _samples #muestras con el mismo orden que el archivo vcf y el pileup

def _getindels_(file=None):
    _samples = []
    _numvar = {}
    _numvar["total"] = 0
    _numvar["indels"] = 0
    _numvar["snps"] = 0
    _output = []
    with open(file, 'r') as f:
        for line in f:
            line = line[:-1]
            if re.match('^##', line) == None:
                if re.match('^#', line) != None:
                    for _sample in line.split('\t')[9:]: _samples.append(_sample)
                    #print "Samples: " + str(_samples)
                else:
                    _numvar["total"] += 1
                    if len(line.split('\t')[3]) != len(line.split('\t')[4]):
                        _mydata = line.split('\t')
                        del _mydata[2]
                        _output.append(_mydata[:4])
                        _numvar["indels"] += 1
                    else: _numvar["snps"] += 1

    #print "Total variants analized: " + str(_numvar["total"])
    print "Total indels found: " + str(_numvar["indels"])
    print "Other variants: " + str(_numvar["snps"])
    return _output, _samples

def _getpileup_(file=None, samples=None):
    try:
        _myhash = {}
        with open(file, 'r') as data:
            for line in data:
                line = line.replace("\r", "").replace("\n", "")
                _mydata = line.split('\t')[:2]
                _myhash = _addkeys_(_myhash, keys=_mydata, value=None)
                n=3
                try:

                    for sample in samples:
                        _mydata = line.split('\t')[:2]
                        _data = {}
                        _data[sample] = {"name":sample,"depth":line.split('\t')[n], "bases":line.split('\t')[n+1]}
                        _mydata.append(_data)
                        n+=3
                        _myhash = _addkeys_(_myhash, keys=_mydata, value="yes")
                except IndexError:
                    continue
        return _myhash

    except IOError or IndexError as e:
        pass
        print '<<ERROR>> Can\'t open file ' + e.strerror + ' ' + str(datetime.now())
        sys.exit(1)

def _addkeys_(self = None, keys = None, value = None):
    _mydict = {}
    if self is not None: _mydict = self
    # print keys
    key = keys.pop(0)
    if key not in _mydict.keys():
        if len(keys) > 0:
            _mydict[str(key)] = _addkeys_(keys = keys, value=value)
        else:
            if value is not None:
                _mydict = key
            else:
                _mydict[str(key)] = {}
    else:
        if len(keys) > 0:
            _mydict[str(key)].update(_addkeys_(self=_mydict[str(key)], keys = keys, value = value))
    return _mydict

def _adddepth(indels=None, pileup=None, samples=None):
    _myvar=''
    _myheader = ["chr","pos","ref","var","allele"]
    _myheader_2 = ["chr","pos","ref","var",  "Mean_freq_var",  "Median_freq_var", "Mean_stbias_ref",  "Median_stbias_ref", "Mean_stbias_var", "Median_stbias_var", "Std_var", "Std_stbias_ref", "Std_stbias_var", "Freq_over_0", "Mean_stbias_ref_vs_var", "Median_stbias_ref_vs_var", "Std_stbias_ref_vs_var" ]
    _myheader_3 = ["chr","    pos","    ref","var"]
    _myheader_4 = ["Sample","Mean_depth", "Mean_freq_var",  "Median_freq_var", "Mean_stbias_ref",  "Median_stbias_ref", "Mean_stbias_var", "Median_stbias_var", "Std_var", "Std_stbias_ref", "Std_stbias_var", "Mean_stbias_ref_vs_var", "Median_stbias_ref_vs_var", "Std_stbias_ref_vs_var"]

    parametros= ["dif_freq_var", "dif_stbias_var", "dif_stbias_ref", "dif_stbias_ref_vs_var", "freq_var", "stbias_ref_vs_var", "Freq_sample","stbiasref_vs_var", "Freq_var_per_sample"]

    for parametro in parametros:

        for sample in samples:
            _myheader_3.append(parametro + " " + sample)



    lines= {}
    lines_2= {}
    lines_3 = {}
    lines_4 = {}




    dictionaries={}

    for sample in samples:
        dictionaries[sample] = {'depths': [],
                                'freq_var_total': [],
                                'stbias_ref': [],
                                'stbias_var': [],
                                'stbias_ref_vs_var': [],
        }

    n=0
    for indel in indels:
        lines[n]=[]
        _mydata = []
        lines_2[n]= []
        _mydata_2 = []
        lines_3[n] = []
        _mydata_3 = []
        if len(indel[3]) > len(indel[2]):
            d = len(indel[3]) - len(indel[2])
            _myvar= "+" + str(d) + indel[3][-d:]
            _mysearchvar = "\+" + str(d) + indel[3][-d:]
        else:
            d = len(indel[2]) - len(indel[3])
            _myvar = "-" + str(d) + indel[2][-d:]
            _mysearchvar = "\-" + str(d) + indel[2][-d:]
        _mydata.extend([indel[0],indel[1],indel[2],_myvar])
        _mydata_2.extend([indel[0], indel[1], indel[2], _myvar])
        _mydata_3.extend([indel[0], indel[1], indel[2], _myvar])

        freq_var_total=[]
        stbias_ref=[]
        stbias_var=[]
        samples_over_0 = 0
        stbias_ref_vs_var = []

        freqvar_vs_freqvartotal= []
        stbiasvar_vs_stbiasvartotal= []
        stbiasref_vs_stbiasreftotal= []
        stbiasrefvsvar_vs_stbiasrefvsvartotal= []

        try:
            for sample in pileup[indel[0]][indel[1]]:
                if (len(lines[0])== 0): _myheader.extend([str(sample +"_depth"), str(sample +"_ref_pos"),str(sample +"_ref_neg"),str(sample +"_var_pos"),str(sample +"_var_neg")])
                [dot, comma, upper, lower] = [0, 0, 0, 0]
                dot=len(re.findall('\.',pileup[indel[0]][indel[1]][sample]['bases']))
                comma=len(re.findall('\,',pileup[indel[0]][indel[1]][sample]['bases']))
                _myvartrans = _mysearchvar.lower()
                upper = len(re.findall(_mysearchvar, pileup[indel[0]][indel[1]][sample]['bases']))
                lower = len(re.findall(_myvartrans, pileup[indel[0]][indel[1]][sample]['bases']))
                _mydata.extend([pileup[indel[0]][indel[1]][sample]['depth'],dot,comma,upper,lower])

                var_total = float(upper) + lower
                ref_total = float(dot) + comma
                depth= float(dot + comma + upper + lower)

                try:
                    freq_var_total.append("{0:.4f}".format(float(upper + lower) / float(dot + comma + upper + lower)))
                except ZeroDivisionError:
                    freq_var_total.append("{0:.4f}".format(float(0)))

                try:
                    stbias_var.append("{0:.4f}".format(float(upper) / float(upper + lower)))
                except ZeroDivisionError:
                    stbias_var.append("{0:.4f}".format(0))
                try:
                    stbias_ref.append("{0:.4f}".format(float(dot) / float(dot + comma)))
                except ZeroDivisionError:
                    stbias_ref.append("{0:.4f}".format(0))

                if var_total > 0:
                    samples_over_0 += 1
                dictionaries[sample]['depths'].append(depth)
                try:

                    dictionaries[sample]['freq_var_total'].append(var_total/depth)
                except ZeroDivisionError:
                    dictionaries[sample]['freq_var_total'].append(0)
                try:
                    stbiasref= float(dot) / ref_total
                except ZeroDivisionError:
                    stbiasref=0
                try:
                    stbiasvar= float(upper) / var_total
                except ZeroDivisionError:
                    stbiasvar=0

                stbiasref_vs_var= np.absolute(stbiasref-stbiasvar)
                stbias_ref_vs_var.append(stbiasref_vs_var)

                dictionaries[sample]['stbias_ref'].append(stbiasref)
                dictionaries[sample]['stbias_var'].append(stbiasvar)

                dictionaries[sample]['stbias_ref_vs_var'].append(stbiasref_vs_var)
        except KeyError:
            continue




        freq_var_total = np.array(freq_var_total).astype(np.float)
        stbias_ref = np.array(stbias_ref).astype(np.float)
        stbias_var = np.array(stbias_var).astype(np.float)

        mean_var_total = "{0:.4f}".format(np.mean(freq_var_total).round(decimals=4))
        median_var_total = "{0:.4f}".format(np.median(freq_var_total).round(decimals=4))

        mean_stbias_ref= "{0:.4f}".format(np.mean(stbias_ref).round(decimals=4))
        median_stbias_ref= "{0:.4f}".format(np.median(stbias_ref).round(decimals=4))

        mean_stbias_var = "{0:.4f}".format(np.mean(stbias_var).round(decimals=4))
        median_stbias_var = "{0:.4f}".format(np.median(stbias_var).round(decimals=4))

        std_var = "{0:.4f}".format(np.std(freq_var_total).round(decimals=4))
        std_stbiasref = "{0:.4f}".format(np.std(stbias_ref).round(decimals=4))
        std_stbiasvar = "{0:.4f}".format(np.std(stbias_var).round(decimals=4))
        freq_samples_over_0 = "{0:.4f}".format((float(samples_over_0) / 16))

        mean_stbias_ref_vs_var = "{0:.4f}".format(np.mean(stbias_ref_vs_var).round(decimals=4))
        median_stbias_ref_vs_var = "{0:.4f}".format(np.median(stbias_ref_vs_var).round(decimals=4))
        std_stbias_ref_vs_var = "{0:.4f}".format(np.std(stbias_ref_vs_var).round(decimals=4))




        for sample in freq_var_total:
            try:

                x =  "{0:.4f}".format(np.absolute(float(sample) - float(mean_var_total)).round(decimals=4))
                freqvar_vs_freqvartotal.append(x)

            except ZeroDivisionError:
                freqvar_vs_freqvartotal.append(0)



        for sample in stbias_var:
            try:

                x = "{0:.4f}".format(float(sample) - float(mean_stbias_var))
                stbiasvar_vs_stbiasvartotal.append(x)

            except ZeroDivisionError:
                stbiasvar_vs_stbiasvartotal.append(0)


        for sample in stbias_ref:
            try:

                x = "{0:.4f}".format(np.absolute(float(sample) - float(mean_stbias_ref)).round(decimals=4))
                stbiasref_vs_stbiasreftotal.append(x)


            except ZeroDivisionError:

                stbiasref_vs_stbiasreftotal.append(0)



        for sample in stbias_ref_vs_var:

            try:

                x = "{0:.4f}".format(np.absolute(float(sample) - float(mean_stbias_ref_vs_var)).round(decimals=4))
                stbiasrefvsvar_vs_stbiasrefvsvartotal.append(x)


            except ZeroDivisionError:

                stbiasrefvsvar_vs_stbiasrefvsvartotal.append(0)





        

        _mydata_2.extend([mean_var_total, median_var_total, mean_stbias_ref, median_stbias_ref, mean_stbias_var, median_stbias_var, std_var, std_stbiasref, std_stbiasvar, freq_samples_over_0, mean_stbias_ref_vs_var, median_stbias_ref_vs_var, std_stbias_ref_vs_var])
        lines[n].extend(_mydata)
        lines_2[n].extend(_mydata_2)

        for sample in freqvar_vs_freqvartotal:
            _mydata_3.append(sample)
        for sample in stbiasvar_vs_stbiasvartotal:
            _mydata_3.append(sample)
        for sample in stbiasref_vs_stbiasreftotal:
            _mydata_3.append(sample)
        for sample in stbiasrefvsvar_vs_stbiasrefvsvartotal:
            _mydata_3.append(sample)

        for sample in freq_var_total:
            _mydata_3.append(sample)

        for sample in stbias_ref_vs_var:
            _mydata_3.append(sample)

        lines_3[n].extend(_mydata_3)




        n+=1

    nn = 0
    for sample in samples:
        _mydata = []
        mean_depth_2="{0:.4f}".format(np.mean(dictionaries[sample]['depths']).round(decimals=4))
        mean_var_total_2 = "{0:.4f}".format(np.mean(dictionaries[sample]['freq_var_total']).round(decimals=4))
        median_var_total_2 = "{0:.4f}".format(np.median(dictionaries[sample]['freq_var_total']).round(decimals=4))
        mean_stbias_ref_2 = "{0:.4f}".format(np.mean(dictionaries[sample]['stbias_ref']).round(decimals=4))
        median_stbias_ref_2 = "{0:.4f}".format(np.median(dictionaries[sample]['stbias_ref']).round(decimals=4))
        mean_stbias_var_2 = "{0:.4f}".format(np.mean(dictionaries[sample]['stbias_var']).round(decimals=4))
        median_stbias_var_2 = "{0:.4f}".format(np.median(dictionaries[sample]['stbias_var']).round(decimals=4))
        std_var_2 = "{0:.4f}".format(np.std(dictionaries[sample]['freq_var_total']).round(decimals=4))
        std_stbias_ref_2 = "{0:.4f}".format(np.std(dictionaries[sample]['stbias_ref']).round(decimals=4))
        std_stbias_var_2 = "{0:.4f}".format(np.std(dictionaries[sample]['stbias_var']).round(decimals=4))
        mean_stbias_ref_vs_var = "{0:.4f}".format(np.mean(dictionaries[sample]['stbias_ref_vs_var']).round(decimals=4))
        median_stbias_ref_vs_var = "{0:.4f}".format(np.median(dictionaries[sample]['stbias_ref_vs_var']).round(decimals=4))
        std_stbias_ref_vs_var = "{0:.4f}".format(np.std(dictionaries[sample]['stbias_ref_vs_var']).round(decimals=4))
        _mydata.extend([sample, mean_depth_2 , mean_var_total_2, median_var_total_2, mean_stbias_ref_2, median_stbias_ref_2, mean_stbias_var_2, median_stbias_var_2, std_var_2, std_stbias_ref_2, std_stbias_var_2, mean_stbias_ref_vs_var, median_stbias_ref_vs_var, std_stbias_ref_vs_var ])
        lines_4[nn] = _mydata
        for indel in lines_3.values():
            indel.append(_mydata[2])
        nn += 1






    #nnn=0

    #for line in lines_2.values():
        #try:
            #_mydata = []
            #_mydata.extend([line[0], line[1], line[2], line[3]])
        #except IndexError:
            #continue
       # for lineq in lines_3.values():
            #median_depth= lineq[1]
            #Dif_mean_freq_global = np.absolute(float(lineq[2]) - float(line[4]))
            #Dif_mean_stbias_var = np.absolute(float(lineq[6]) - float(line[8]))
            #Dif_mean_stbias_ref = np.absolute(float(lineq[4]) - float(line[6]))
            #Dif_stbiasref_stbiasvar = np.absolute(float(lineq[13])-float(line[16]))
            #_mydata.extend([median_depth, "{0:.4f}".format(Dif_mean_freq_global.round(decimals=4)), "{0:.4f}".format(Dif_mean_stbias_var.round(decimals=4)), "{0:.4f}".format(Dif_mean_stbias_ref.round(decimals=4)), "{0:.4f}".format(Dif_stbiasref_stbiasvar.round(decimals=4))])
        #lines_4[nnn]=_mydata
        #nnn+=1



    #print ("\t".join(_myheader) + "\n")
    #for line in lines:
        #print('\t'.join(map(str, lines[line])) + "\n")

    with open("data.txt", 'w+') as f:
        f.write(str("\t".join(_myheader)) + "\n")
        for line in lines:
            f.write(str('\t'.join(map(str, lines[line]))) + "\n")
        f.close()

    #print ("\t".join(_myheader_2) + "\n")
    #for line in lines_2:
        #print('\t'.join(map(str, lines_2[line])))

    with open("Stats_position.txt", 'w+') as f:
        f.write(str("\t".join(_myheader_2)) + "\n")
        for line in lines_2:
            f.write(str('\t'.join(map(str, lines_2[line]))) + "\n")
        f.close()

    #print "\t".join(_myheader_3)
    #for line in lines_3:
        #print('\t'.join(map(str, lines_3[line])))

    with open("Stats_dif.txt", 'w+') as f:
        f.write(str("\t".join(_myheader_3)) + "\n")
        for line in lines_3:
            f.write(str('\t'.join(map(str, lines_3[line]))) + "\n")
        f.close()

    #print "\t".join(_myheader_4)
    #for line in lines_4:
        #print('\t'.join(map(str, lines_4[line])))

    with open("Stats_sample.txt", 'w+') as f:
        f.write(str("\t".join(_myheader_4)) + "\n")
        for line in lines_4:
            f.write(str('\t'.join(map(str, lines_4[line]))) + "\n")
        f.close()

    return lines_3

def _new_pileup(data=None, file=None, samples= None):
    ultimate_indels = []
    for indel in data.values():
        indel_counter = 0
        n = 0
        for sample in samples:
            try:
                dif_freq = indel[4 + n]
                stbiasref_vs_var = indel[84 + n]
                freq = float(indel[68 + n])
                freqsample = float(indel[100 + n])
                freq_vs_freqsample = freq - freqsample
                if dif_freq >= 0.20 and stbiasref_vs_var <= 0.15 and freq_vs_freqsample > 0.10: indel_counter = indel_counter + 1

            except IndexError:
                pass

            n += 1

        if indel_counter > 0:
            ultimate_indels.append(indel[1]+indel[3])



    print "Ultimate indels: " + str(len(ultimate_indels))
    Ultimate_Indels = 0

    with open("New_vcf_file.txt", 'w+') as nf:
        with open(file, "r") as f:
            for line in f:
                _myvar=''
                line = line[:-1]
                if (re.match('^##', line) != None) or (re.match('^#', line) != None):
                    nf.write(line + '\n' )
                else:
                    if len(line.split('\t')[3]) == len(line.split('\t')[4]):
                        nf.write(line + '\n')
                        Ultimate_Indels += 1

                    else:
                        if len(line.split("\t")[4]) > len(line.split("\t")[3]):
                            d = len(line.split("\t")[4]) - len(line.split("\t")[3])
                            _myvar = str("+" + str(d) + line.split("\t")[4][-d:])

                        else:
                            #len(line.split("\t")[4]) < len(line.split("\t")[3]):
                            d = len(line.split("\t")[3]) - len(line.split("\t")[4])
                            _myvar = str("-" + str(d) + line.split("\t")[3][-d:])

                        if (line.split("\t")[1] + _myvar) in ultimate_indels:
                            nf.write(line + '\n')
                            Ultimate_Indels +=1

    print "Ultimate Variants: " + str(Ultimate_Indels)




def _main_(argv):
    if not argv:
        print 'start.py -v <vcf file> -s <sample file> -p <pileup file> -o <output file>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hv:s:p:o:")

    except getopt.GetoptError:
        print 'start.py -v <vcf file> -s <sample file> -p <pileup file> -o <output file>'
        sys.exit(2)

    vcf_file, sam_file, pil_file, output_file= '', '', '', ''
    for opt, arg in opts:
        if opt == '-h':
            print 'start.py -v <vcf file> -s <sample file> -p <pileup file> -o <output file>'
            sys.exit()
        elif opt == '-v':
            vcf_file = arg
        elif opt == '-s':
            sam_file = arg
        elif opt == '-p':
            pil_file = arg
        elif opt == '-o':
            output_file = arg
    if vcf_file == '' or sam_file == '' or pil_file == '':
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + str(datetime.now())
    _myindels, _mysamples = _getindels_(file=vcf_file)
    _mypileup = _getpileup_(file=pil_file, samples=_mysamples)
    _myindels = _adddepth(indels=_myindels, pileup=_mypileup, samples=_mysamples)
    _my_new_pileup = _new_pileup(data=_myindels, file=vcf_file, samples= _mysamples)

    print "Process finished... " + str(datetime.now())

    sys.exit(0)



_main_(sys.argv[1:])


# _myindels, _mysamples = _getindels_(file="vcf_file.vcf")
# _mypileup = _getpileup_(file="pil_file.pileup", samples=_mysamples)
# _myindels = _adddepth(indels=_myindels, pileup=_mypileup, samples=_mysamples)
# _my_new_pileup = _new_pileup(data=_myindels, file="vcf_file.vcf", samples= _mysamples)
















