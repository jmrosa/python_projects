#!/usr/bin/env python
"""
SYNOPSIS
    variant_fix_calling.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    variant_fix_calling.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
from tools import *

def _help():
        print 'variant_fix_calling.py -p <pileup file> -l <list file> -o <output file>'
        print "####################\n"
        sys.exit(2)

def _process(file=None, samples=None, output=None):
    print file
    print samples
    print output
    with open(file, "r") as f:
        for line in f:
            _mydata=line[:-1].split('\t')
            while len(_mydata) < (int(samples) + 1) * 3:
                print _mydata
                _mydata.append('0\t*\t*')


def _main_(argv):
    print "\n####################"
    print "variant_fix_calling.py v0.1\n"

    if not argv:
        _help()
    try:
      opts, args = getopt.getopt(argv,"hp:l:o:")

    except getopt.GetoptError:
        _help()

    pileup_file, list_file, output = '','','tmp'
    for opt, arg in opts:
        if opt == '-h':
            _help()
        elif opt == '-p':
            pileup_file = arg
            #print 'Control file is ', control_file
        elif opt == '-l':
            list_file = arg
            #print 'Case file is ', case_file
        elif opt == '-o':
            out_file = arg
            #print 'Ouput name is ', output

    if pileup_file == '' or list_file == '': _help()

    print "Process starts... " + _gettime_()
    _mynumber = _commandout_(command="wc -l %s" %list_file).split(" ")[0]
    _process(file=pileup_file,samples=_mynumber,output=out_file)
    print "Process finished... " + _gettime_()
    print "####################\n"

    sys.exit(0)



_main_(sys.argv[1:])