#!/usr/bin/env python
"""
SYNOPSIS
    split_vcf.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to split variants into lines in vcf file
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    split_vcf.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import re
from tools import _gettime_

def _main_(argv):
    if  not argv:
        print 'split_vcf.py -v <vcf file> -o <output file>'
        sys.exit(1)
    try:
      opts, args = getopt.getopt(argv,"hv:a:o:")

    except getopt.GetoptError:
        print 'split_vcf.py -v <vcf file> -o <output file>'
        sys.exit(2)

    vcf_file, output_file= '', ''
    for opt, arg in opts:
        if opt == '-h':
            print 'split_vcf.py -v <vcf file> -o <output file>'
            sys.exit()
        elif opt == '-v':
            vcf_file = arg
        elif opt == '-o':
            output_file = arg
    if vcf_file == '':
        print "<<ERROR>> not input files"
        sys.exit(1)

    print "Process starts... " + _gettime_()

    _myoutput = open(str(output_file), "w")
    with open(str(vcf_file), "r") as f:
        for line in f:
            line = line[:-1]
            if re.match('^#', line) != None:
                _myoutput.write(line + '\n')
            else:
                if re.search(',', line.split('\t')[4]) != None:
                    _index = 1
                    _alleles = {}
                    _mynewline = line.split('\t')
                    for n in _mynewline[4].split(','):
                        if n not in _alleles.keys():
                            _alleles[n] = _index
                        else:
                            _alleles[n] = str(_alleles[n]) + "," + str(_index)
                        _index += 1
                    for allele in _alleles:
                        _mynewline[4] = allele
                        for sample in range(9, len(_mynewline)):
                            _myinfo = line.split('\t')[sample].split(':')
                            _result = 0
                            for _index in str(_alleles[allele]).split(','):
                                if re.search (str(_index), _myinfo[0]) != None:
                                    _result = 1

                            if _myinfo[0] != './.' and _myinfo[0] != '0/0' and _result == 0:
                                _myinfo[0] = '0/0'
                                _myinfo[5] = '0'
                                _myinfo[6] = '0.0%'
                                _myinfo[7] = '9.8E-1'
                                _myinfo[12] = '0'
                                _myinfo[13] = '0'
                            else:
                                for _index in str(_alleles[allele]).split(','):
                                    _myinfo[0] = _myinfo[0].replace(str(_index), '1')

                            _mynewline[sample] = ':'.join(_myinfo)
                        _myoutput.write('\t'.join(_mynewline)+'\n')

                else:
                    _myoutput.write(line + '\n')

    print "Process finished... " + _gettime_()
    sys.exit(0)


_main_(sys.argv[1:])