#!/usr/bin/env python
"""
SYNOPSIS
    vcftools.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Module for commonly vcf used tools
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    vcftools.py v0.1
"""
__author__ = 'jmrosa'

__all__ = ['_getvariants_','_getvariantshapmap_','_getvariantsinhouse_','_printsummary_']

from tools import *
import sys
import os
import re


def _getvariants_(sampfile='', bedfile='', name=''):
    path = os.getcwd()
    _filename = path + '/' + name + '_bedtools.vcf'
    _command = 'grep -v "^##" %s | grep "^#"  > %s 2> %s' %(sampfile, _filename, _filename +'.log')
    results = _command_(command=_command)
    if results != 0:
        print "<<ERROR>> Launching command \n"
        print _command
        sys.exit(1)
    _command = 'bedtools intersect -a %s -b %s >> %s 2>> %s' %(sampfile, bedfile, _filename, _filename +'.log')
    results = _command_(command=_command)
    if results != 0:
        print "<<ERROR>> Launching command \n"
        print _command
        sys.exit(1)
    return _filename

def _getcolnames_(line=None, names=None):
    cols = line.split('\t')
    _mycols = {}
    for name in names:
        for n in range(len(cols)):
            if re.match(name, cols[n]) != None:
                _mycols[cols[n]] = n
    return _mycols

def _getvariantshapmap_(samples=None, variants_file=None):
    _myvariants={}
    try:
        with open(variants_file, 'r') as f:
            _mycols = {'hapmap':{}}
            for line in f:
                line = line.replace('\n','')
                if re.search('^#CHROM', line) != None: _mycols['hapmap'].update(_getcolnames_(line=line,names=samples))
                else:
                    data = line.split('\t')
                    _myvars = data[4].split(',')
                    var = 1
                    for variant in _myvars:
                        for name in _mycols['hapmap'].keys():
                            _myvariants.update(_addkeys_(self=_myvariants,keys=[str(data[0]),str(data[1]),str(data[3]),variant,'hapmap', name]))
                            _mydata = data[_mycols['hapmap'][name]]
                            if re.match('\.', _mydata) == None:
                                _mygeno = _mydata.split(':')[0]
                                _mydepth = _mydata.split(':')[2]
                                if re.search(str(var), _mygeno) != None:
                                    _mygeno = _mygeno.replace('0', str(data[3]))
                                    _mygeno = _mygeno.replace(str(var), variant)
                                else:
                                    _mygeno = '/'.join([str(data[3]),str(data[3])])
                                _var = _mydata.split(':')[1].split(',')[var]
                                _myfreq = "{0:.2f}".format(float(_var) / float(_mydepth))
                            else:
                                _mygeno = './.'
                                _mydepth = '-'
                                _myfreq = '-'
                            _myvariants[str(data[0])][str(data[1])][str(data[3])][variant]['hapmap'][name]['genotype'] = _mygeno
                            _myvariants[str(data[0])][str(data[1])][str(data[3])][variant]['hapmap'][name]['depth'] =_mydepth
                            _myvariants[str(data[0])][str(data[1])][str(data[3])][variant]['hapmap'][name]['freq'] = _myfreq
                    var += 1

        return (_myvariants, _mycols)

    except IOError as e:
        print e
        print '<<ERROR>>: cannot open %s' %variants_file
        sys.exit(1)

def _getvariantsinhouse_(samples=None, variants_file=None, hapmap=None):
    try:
        with open(variants_file, 'r') as f:
            _mycols={'inhouse':{}}
            for line in f:
                line = line.replace('\n','')
                if re.search('^#CHROM', line) != None:
                    _mycols['inhouse'].update(_getcolnames_(line=line,names=samples))
                else:
                    data = line.split('\t')
                    for name in _mycols['inhouse'].keys():
                        hapmap.update(_addkeys_(self=hapmap,keys=[str(data[0]),str(data[1]),str(data[3]),str(data[4]),'inhouse',name]))
                        _mydata = data[_mycols['inhouse'][name]]
                        if re.match('\.', _mydata) == None:
                            _mygeno = _mydata.split(':')[0]
                            _mygeno = _mygeno.replace('0', str(data[3]))
                            _mygeno = _mygeno.replace('1', str(data[4]))
                            _mydepth = _mydata.split(':')[3]
                            _myfreq = "{0:.2f}".format(float(_mydata.split(':')[5]) / (float(_mydata.split(':')[3])))
                        else:
                            _mygeno = './.'
                            _mydepth = '-'
                            _myfreq = '-'

                        hapmap[str(data[0])][str(data[1])][str(data[3])][str(data[4])]['inhouse'][name]['genotype'] = _mygeno
                        hapmap[str(data[0])][str(data[1])][str(data[3])][str(data[4])]['inhouse'][name]['depth'] =_mydepth
                        hapmap[str(data[0])][str(data[1])][str(data[3])][str(data[4])]['inhouse'][name]['freq'] = _myfreq
        return (hapmap, _mycols)

    except Exception as e:
        print e
        print '<<ERROR>>: cannot open %s' %variants_file
        sys.exit(1)


def _printsummary_(data=None, output=None, samples=None):
    path = os.getcwd()
    _filename = path + '/' + output + '_results.csv'
    _createfile_(_filename, ';'.join(['#Chr','position','ref','var','']))
    n=1
    for chr in data.keys():
        for position in data[chr].keys():
            for ref in data[chr][position].keys():
                for var in data[chr][position][ref].keys():
                    samplesinfo=[]
                    info = [chr,position,ref,var]
                    if 'hapmap' in data[chr][position][ref][var].keys():
                        for sample in sorted(samples['hapmap'], key=str.lower):
                            samplesinfo.extend([sample+'_genotype_hapmap',sample+'_freq_hapmap',sample+'_depth_hapmap'])
                            info.extend([data[chr][position][ref][var]['hapmap'][sample]['genotype'],data[chr][position][ref][var]['hapmap'][sample]['freq'],data[chr][position][ref][var]['hapmap'][sample]['depth']])
                            for sample1 in sorted(samples['inhouse'],key=str.lower):
                                if re.match(sample, sample1) != None:
                                    samplesinfo.extend([sample1+'_genotype_inhouse',sample1+'_freq_inhouse',sample1+'_depth_inhouse'])
                                    if 'inhouse' in data[chr][position][ref][var].keys():
                                        info.extend([data[chr][position][ref][var]['inhouse'][sample1]['genotype'],data[chr][position][ref][var]['inhouse'][sample1]['freq'],data[chr][position][ref][var]['inhouse'][sample1]['depth']])
                                    else:
                                        info.extend(['-','-','-'])

                        if n==1: _appendfile_(_filename,';'.join(samplesinfo) + '\n')
                        _appendfile_(_filename,';'.join(info) + '\n')
                    else:
                        for sample in sorted(samples['hapmap'], key=str.lower):
                            samplesinfo.extend([sample+'_genotype_hapmap',sample+'_freq_hapmap',sample+'_depth_hapmap'])
                            info.extend(['-','-','-'])
                            for sample1 in sorted(samples['inhouse'],key=str.lower):
                                if re.match(sample, sample1) != None:
                                    samplesinfo.extend([sample1+'_genotype_inhouse',sample1+'_freq_inhouse',sample1+'_depth_inhouse'])
                                    info.extend([data[chr][position][ref][var]['inhouse'][sample1]['genotype'],data[chr][position][ref][var]['inhouse'][sample1]['freq'],data[chr][position][ref][var]['inhouse'][sample1]['depth']])
                        if n==1: _appendfile_(_filename,';'.join(samplesinfo) + '\n')
                        _appendfile_(_filename,';'.join(info) + '\n')
                    n+=1
    return 1
