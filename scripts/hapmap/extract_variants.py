#!/usr/bin/env python
"""
SYNOPSIS
    extract_variants.py [-h,--help] [--verbose] [--version]
DESCRIPTION
    Script to merge variant information with annotation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    correlations.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import re
from vcftools import *
from tools import *

def _help():
        print 'extract_variants.py -c <Control vcf file> -v <Cases vcf file> -b <bed file> -n <names> -p <pedigree file> -o <root name> -t <number of threads>'
        print "- names = comma separated -"
        print "- root name is optional -"
        print "- default number of threads = 8 -"
        print "####################\n"
        sys.exit(2)


def _main_(argv):
    print "\n####################"
    print "extract_variants.py v0.1\n"

    if not argv:
        _help()
    try:
      opts, args = getopt.getopt(argv,"hc:v:n:o:b:p:t:")

    except getopt.GetoptError:
        _help()

    control_file, case_file, names, bed_file, ped_file, output, threads = '','','','','','tmp',8
    for opt, arg in opts:
        if opt == '-h':
            _help()
        elif opt == '-c':
            control_file = arg
            #print 'Control file is ', control_file
        elif opt == '-v':
            case_file = arg
            #print 'Case file is ', case_file
        elif opt == '-n':
            names = arg.split(',')
            #print 'Names are ', names
        elif opt == '-b':
            bed_file = arg
            #print 'Bed file is ', bed_file
        elif opt == '-p':
            ped_file = arg
            #print 'Pedigree file is ', ped_file
        elif opt == '-o':
            output = arg
            #print 'Ouput name is ', output
        elif opt == '-t':
            threads = arg
            #print 'Nmber of threads is ', threads

    if control_file == '' or case_file == '' or names == '' or bed_file == '': _help()

    print "Process starts... " + _gettime_()
    _file = _getvariants_(sampfile=control_file, bedfile=bed_file, name=output+'_hapmap')
    (_variants, _samples) = _getvariantshapmap_(samples=names, variants_file=_file)
    _file = _getvariants_(sampfile=case_file, bedfile=bed_file, name=output+'_inhouse')
    (_results, _inhousesamples)  = _getvariantsinhouse_(samples=names, variants_file=_file, hapmap=_variants)
    _samples.update(_inhousesamples)
    _printsummary_(data=_results, output=output, samples=_samples)
    print "Process finished... " + _gettime_()
    print "####################\n"

    sys.exit(0)



_main_(sys.argv[1:])