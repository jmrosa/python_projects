#!/usr/bin/env python
"""
SYNOPSIS
    essay.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Module for essay creation
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    essay.py v0.1
"""
__author__ = 'jmrosa'

import sys
import os
import traceback
import json
import ast
from tools import *
import re

def CreateEssay(json, path=None, name=None):
    """ Function to create the essay object"""
    path = path + "/Analysis/" + name
    state = str(json["Step"]["step"])
    command = json["Step"]["command"]
    pipeline = json["pipeline"]
    essay = Essay(state=state, command=command, pipeline=pipeline, name=name, path=path)
    if "sample_file" in pipeline.keys(): essay.samplelist = pipeline["sample_file"]
    for sample in json["samples"]:
        essay._addsample_(name=sample, json=json["samples"][sample], state=state, command=command, pipeline=pipeline)
    essay._getpipeline_(pipeline)
    essay._readstate_(state, command)
    return essay


# Class definitions
class Essay(object):
    """Class essay to create essay objects"""

    def __init__(self, state=None, command=None, pipeline=None, name=None, path=None):
        self.name = name
        self.ID = name
        self.files = {}
        self.path = path
        self._createpath_(path)
        self.samples = {}
        self.lastfile = {'aux':'','auxformat':'','name':'','format':''}
        self.sequencer = pipeline["sequencer"]
        self.capture_tech = pipeline["capture_tech"]
        self.samplelist = 'default'

    def _addfile_(self, step=None, type=None, dir=None, name=None):
        if type not in self.files.keys():
            self.files[type] = {}
        if dir not in self.files[type].keys():
            self.files[type][dir] = {}
        if str(step) not in self.files[type][dir].keys():
            self.files[type][dir][str(step)] = {}
        path = self.path + "/" + type + "/" + dir + "/" + str(step)
        self._createpath_(path)
        _file = path + "/" + name
        self.files[type][dir][str(step)] = _file
        return _file

    def _addsample_(self, name=None, json=None, state=None, command=None, pipeline=None):
        self.samples[name] = Sample(name, json, self.path, state, command, pipeline)

    def _getpipeline_(self, pipeline=None):
        with open(str(pipeline["file"]), "r") as f:
            _pipeline = json.load(f)
        self.pipeline = _addkeys_(keys=["genome"])
        self.pipeline["genome"]["fasta"] = _pipeline["genomes"][pipeline["genome"]]["fasta"]
        self.pipeline["pipelines_path"] = _pipeline["pipelines_path"]
        self.pipeline["genome"]["novoindex"] = _pipeline["genomes"][pipeline["genome"]]["novoindex"]
        self.pipeline["genome"]["species"] = _pipeline["genomes"][pipeline["genome"]]["species"]
        self.pipeline["capture_files"] = _pipeline["capture_techs"][self.capture_tech["kit"]][self.capture_tech["panel"]]["files"][pipeline["genome"]]
        self.pipeline["aligner"] = pipeline["aligner"]
        self.pipeline["version"] = pipeline["genome"]

        for phase in range(1, len(_pipeline["capture_techs"][self.capture_tech["kit"]][self.capture_tech["panel"]]["analysis"].keys()) + 1):
            step = str(_pipeline["capture_techs"][self.capture_tech["kit"]][self.capture_tech["panel"]]["analysis"][str(phase)]["name"])
            for key in ["next", "previous", "steps", "commands"]:
                self.pipeline.update(_addkeys_(self=self.pipeline, keys=["Phases", step, key]))

            if str(phase - 1) in ast.literal_eval(json.dumps(_pipeline["capture_techs"][self.capture_tech["kit"]][self.capture_tech["panel"]]["analysis"])).keys():
                #print "we have next for " + step
                self.pipeline["Phases"][step]["previous"] = str(_pipeline["Phases"][step]["previous"])
            else:
                #print "we have no next for " + step
                self.pipeline["Phases"][step]["previous"] = None

            if str(phase + 1) in ast.literal_eval(json.dumps(_pipeline["capture_techs"][self.capture_tech["kit"]][self.capture_tech["panel"]]["analysis"])).keys():
                #print "we have next for " + step
                self.pipeline["Phases"][step]["next"] = str(_pipeline["Phases"][step]["next"])
            else:
                #print "we have no next for " + step
                self.pipeline["Phases"][step]["next"] = None

            for pair in str(_pipeline["capture_techs"][self.capture_tech["kit"]][self.capture_tech["panel"]]["analysis"][str(phase)]["steps"]).split(','):
                self.pipeline["Phases"][step]["steps"].update(_addkeys_(self=self.pipeline["Phases"][step]["steps"], keys=[pair.split(':')[0]]))
                self.pipeline["Phases"][step]["steps"][pair.split(':')[0]] = str(self.pipeline["pipelines_path"] + '/' +  pair.split(':')[1] + ".json")

            for com in range(1, int(len(self.pipeline["Phases"][step]["steps"]) + 1)):
                with open(self.pipeline["Phases"][step]["steps"][str(com)], "r") as f:
                    _pipeinfo = json.load(f)
                    command_info = _pipeinfo["Parameters"]["default"]
                if pipeline[step]["Steps"][str(com)]["parameters"] != 'default': command_info = pipeline[step]["Steps"][str(com)]["parameters"]
                _mycommand = command_info["Command"]
                _persample = ''
                if "Options" in command_info.keys():
                    for option in command_info["Options"].keys():
                        if option == "Persample":
                             for sample in self.samples:
                                _persample += " " + command_info["Options"][option]
                                _persample = _persample.replace('_samplename_', self.samples[sample].name)
                        else:
                            _mycommand += " " + command_info["Options"][option]
                _mycommand = _mycommand.replace('_persample_', _persample)
                _mycommand += " " + command_info["Output"]
                if "Special" in command_info.keys(): _mycommand += " " + command_info["Special"]
                _mycommand += " " + command_info["Log"]
                self.pipeline.update(_addkeys_(self.pipeline, keys=["Phases", step, "commands", str(com)]))
                self.pipeline["Phases"][step]["commands"][str(com)]["command"] = _mycommand.replace('_fasta_', self.pipeline["genome"]["fasta"])
                self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace('_novoindex_', self.pipeline["genome"]["novoindex"])
                self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("xxx", "<")
                if "covered" in self.pipeline["capture_files"].keys() : self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_regions_", self.pipeline["capture_files"]["covered"])
                if "canonics" in self.pipeline["capture_files"].keys(): self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_canonicsfile_", self.pipeline["capture_files"]["canonics"])
                else: self.pipeline["Phases"][step]["commands"][str(com)]["command"]= self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace('-c _canonicsfile_','')
                if "falses" in self.pipeline["capture_files"].keys(): self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_falposfile_", self.pipeline["capture_files"]["falses"])
                else: self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace('-k _falposfile_','')
                if "pathogenic" in self.pipeline["capture_files"].keys(): self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_patfile_", self.pipeline["capture_files"]["pathogenic"])
                else: self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace('-p _patfile_','')
                if "varbedfile" in self.pipeline["capture_files"].keys(): self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_varbedfile_", self.pipeline["capture_files"]["varbedfile"])
                if self.samplelist is not 'default': self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_samplesfile_", self.samplelist)
                self.pipeline["Phases"][step]["commands"][str(com)]["command"] = self.pipeline["Phases"][step]["commands"][str(com)]["command"].replace("_version_", self.pipeline["version"])
                self.pipeline["Phases"][step]["commands"][str(com)]["output_format"] = command_info["Output_format"]
                if "Samples" in _pipeinfo: self.pipeline["Phases"][step]["commands"][str(com)]["samples"] = _pipeinfo["Samples"]
                if "Resample" in _pipeinfo: self.pipeline["Phases"][step]["commands"][str(com)]["resample"] = _pipeinfo["Resample"]
                if "Merge" in _pipeinfo: self.pipeline["Phases"][step]["commands"][str(com)]["merge"] = _pipeinfo["Merge"]
                if "TmpOutput" in _pipeinfo: self.pipeline["Phases"][step]["commands"][str(com)]["tmpoutput"] = _pipeinfo["TmpOutput"]
                if "AuxFile" in _pipeinfo: self.pipeline["Phases"][step]["commands"][str(com)]["auxfile"] = _pipeinfo["AuxFile"]
                if "Threads" in _pipeinfo: self.pipeline["Phases"][step]["commands"][str(com)]["threads"] = _pipeinfo["Threads"]
                else: self.pipeline["Phases"][step]["commands"][str(com)]["threads"] = 1
        for sample in self.samples:
            self.samples[sample].pipeline = self.pipeline

    def _readstate_(self, step=None, command=None):
        try:
            with open(self._addfile_(type="state", dir="global", step="tmp", name=self.name + "_state.json"), "r") as state_file:
                self.state = json.load(state_file)
        except Exception as e:
            self._writelog_("<<WARNING>>: state file could not be opened for " + self.name)
            self._writelog_("<<WARNING>>: creating file with default values")
            self.state = _addkeys_(keys=["essay_name"])
            self.state["origin"] = step
            self.state["essay_name"] = self.name
            self.state.update(_addkeys_(self.state, keys=["current"]))
            self.state["current"]["step"] = step
            self.state["current"]["command"] = str(command)
            self.state.update(_addkeys_(self.state, keys=[step]))
            self.state[step]["global"] = "pending"
            self.state[step][str(command)] = "pending"
            self.state["complete"] = "pending"
            self.state["start"] = _gettime_()

    def _writestate_(self, step=None, exit=None, command=None):
        if exit is "finished":
            if str(command) in self.state["current"].keys(): del self.state["current"][str(command)]
            if self.pipeline["Phases"][step]["next"] != None:
                self.state[step][str(command)] = "finished"
                self.state["complete"] = 'in progress'
                self.state["end"] = _gettime_()
                if int(command) < len(self.pipeline["Phases"][step]["commands"].keys()):
                    self.state["current"]["command"] = str(sorted(self.pipeline["Phases"][step]["commands"].keys())[command])
                    self.state["current"][str(sorted(self.pipeline["Phases"][step]["commands"].keys())[command])] = "pending"
                    self.state[step][str(sorted(self.pipeline["Phases"][step]["commands"].keys())[command])] = "pending"
                    self.state[step]["global"] = "in progress"
                else:
                    self.state[step]["global"] = "finished"
                    self.state["current"]["step"] = str(self.pipeline["Phases"][step]["next"])
                    self.state["current"][sorted(self.pipeline["Phases"][self.state["current"]["step"]]["commands"].keys())[0]] = "pending"
                    self.state["current"]["command"] = sorted(self.pipeline["Phases"][self.state["current"]["step"]]["commands"].keys())[0]
                    self.state[self.state["current"]["step"]] = {}
                    self.state[self.state["current"]["step"]]["global"] = "pending"
                    self.state[self.state["current"]["step"]][str(sorted(self.pipeline["Phases"][self.state["current"]["step"]]["commands"].keys())[0])] = "pending"
            else:
                self.state[step][str(command)] = "finished"
                self.state["end"] = _gettime_()
                if int(command) <  len(self.pipeline["Phases"][step]["commands"].keys()):
                    self.state["current"][sorted(self.pipeline["Phases"][step]["commands"].keys())[command]] = "pending"
                    self.state[step]["global"] = "in progress"
                    self.state["current"][str(int(sorted(self.pipeline["Phases"][step]["commands"].keys())[command]))] = "pending"
                    self.state["current"]["command"] = sorted(self.pipeline["Phases"][step]["commands"].keys())[command]
                    self.state[str(step)][str(int(sorted(self.pipeline["Phases"][step]["commands"].keys())[command]))] = "pending"
                    self.state["complete"] = 'in progress'

                else:
                    self.state[step]["global"] = "finished"
                    self.state["complete"] = 'finished'
                    self.state["current"]["step"] = step
                    self.state["current"][str(command)] = 'finished'
        else:
            self.state[step]["global"] = "pending"
            self.state[step][str(command)] = "pending"
            self.state["current"]["step"] = step
            self.state["current"][str(command)] = "pending"
            self.state["complete"] = 'in progress'

        try:
            file = self._addfile_(type = "state", dir = "global", step = "tmp", name = self.name + "_state.json")
            with open(file, 'w') as state_file:
                json.dump(self.state, state_file, sort_keys=True, indent=4, separators=(',', ': '))
        except Exception as e:
                self._writelog_('<<ERROR>>: cannot write state to ' + file)
                self._writelog_(str(e))
                traceback.print_exc()
                sys.exit(1)

    def _createpath_(self, path=None):
        if path == None:
            self._writelog_("<<ERROR>>: Not a proper path for " + self.name)
        elif not os.path.exists(path):
            os.makedirs(path)
            self._writelog_("<<NOTE>>: " + path + " successfully created.")

    def _writelog_(self, text=None):
        self._addfile_(type="log", dir="global", step="tmp", name=_gettime_("files") + "_" + self.name + "_global.log")
        with open(self.files["log"]["global"]["tmp"], 'a+') as file:
            file.write(str(text) + "\n")

    def _launch_(self):
        step = self.state["origin"]
        while self.state["complete"] != "finished":
                for com in range(int(self.pipeline["Phases"][step]["commands"].keys()[0]), len(self.pipeline["Phases"][step]["commands"]) + 1):
                    _mycommands = []
                    if "samples" in self.pipeline["Phases"][step]["commands"][str(com)].keys():
                        samples = {}
                        _number = 0
                        for sample in self.samples.keys():
                            if step not in self.samples[sample].state.keys():
                                self.samples[sample].state[step] = {}
                                self.samples[sample].state[step][str(com)] = "pending"
                            if "resample" in self.pipeline["Phases"][step]["commands"][str(com)].keys():
                                _command = self.samples[sample]._getcommand_(step=step, com=com, pipeline=self.pipeline, input=self.lastfile["name"])
                            else:
                                _command = self.samples[sample]._getcommand_(step=step, com=com, pipeline=self.pipeline)
                            if self.samples[sample].state[step][str(com)] != "finished":
                                samples[_number] = sample
                                _number += 1
                                _mycommands.append(_command)
                        results = _parallel_(commands=_mycommands, threads=self.pipeline["Phases"][step]["commands"][str(com)]["threads"])
                        _error = ''
                        for n in samples.keys():
                            print "Command %s: Analising sample number %s of %s" %(com, n, len(self.samples.keys()))
                            sample =  samples[n]
                            if results[n] != 0:
                                self.samples[sample]._writestate_(step, "error", com)
                                self._writelog_("<<ERROR>> while launching command: %s" %_mycommands[n])
                                _error = "yes"
                            else:
                                self.samples[sample]._writestate_(step, "finished", com)
                        if _error != "yes":
                            if self.state[step][str(com)] != "finished": self._writestate_(step=step, exit="finished", command=com)
                        else:
                            return "error"
                    else:
                        if "merge" in self.pipeline["Phases"][step]["commands"][str(com)].keys():
                            _mycommand = self._merge_(step=step, com=com)
                        else:
                            _mycommand = self._getcommand_(step=step, com=com, pipeline=self.pipeline)
                        if self.state[step][str(com)] != "finished":
                            #print "Launching command \n %s" %(_mycommand)
                            _myresult = _command_(_mycommand)
                            if _myresult != 0:
                                self._writestate_(step, _myresult, com)
                                self._writelog_("<<ERROR>> while launching command: %s" %_mycommand)
                                return "error"
                            else:
                                self._writestate_(step, "finished", com)
                                for sample in self.samples.keys():
                                    self.samples[sample]._writestate_(step, "finished", com)

                step = self.pipeline["Phases"][step]["next"]
        else: return "finished"

    def _merge_(self, step = None, com = None):
        _mycommand = self.pipeline["Phases"][step]["commands"][str(com)]["command"]
        _match = "_inputfile_"
        _mykey = _getmatch_(match=_match, text=_mycommand)
        _myinput = ''

        self.lastfile["aux"] = self._addfile_(type="trashbin", dir=step, step="step" + str(com), name=self.name + "_" + step + "_step" + str(com) + ".samplelist")
        with open(str(self.lastfile["aux"]), "w") as f:
            for sample in self.samples:
                _myinput += _mykey + self.samples[sample].lastfile["name"] + ' '
                f.write(self.samples[sample].name + "\n")
        _myinput = _replace_(match="^"+_mykey, text=_myinput)

        self.files.update(_addkeys_(self.files, keys=["inputfiles", "analysis", step, str(com)]))
        self.files["inputfiles"]["analysis"][step][str(com)]["name"] = _myinput
        self.files["inputfiles"]["analysis"][step][str(com)]["format"] = self.samples[sample].lastfile["format"]

        return self._getcommand_(pipeline=self.pipeline, step=step, com=com)

    def _getinputinfo(self, step=None, command=None):
        if "inputfiles" in self.files.keys() and step in self.files["inputfiles"]["analysis"] and str(command) in self.files["inputfiles"]["analysis"][step].keys():
            return self.files["inputfiles"]["analysis"][step][str(command)]["name"], self.files["inputfiles"]["analysis"][step][str(command)]["format"], '', ''
        else:
            return self.lastfile["name"], self.lastfile["format"], self.lastfile["aux"], self.lastfile["auxformat"]

    def _getcommand_(self, pipeline=None, step=None, com=None, input=None):
        _type = "analysis"

        if "tmpoutput" in pipeline["Phases"][step]["commands"][str(com)]:
            _type = "trashbin"
        if pipeline["Phases"][step]["commands"][str(com)]["output_format"] == "dir":
            _myoutput = self._addfile_(type=_type, dir=step, step="step" + str(com), name=self.name + '/' + self.name + "_Aligned.sortedByCoord.out.bam")
            _mydir = self._addfile_(type=_type, dir=step, step="step" + str(com), name=self.name)
            self._createpath_(_mydir)
        else:
            _myoutput = self._addfile_(type=_type, dir=step, step="step" + str(com),
                                     name=self.name + "_" + step + "_step" + str(com) + "." +
                                          pipeline["Phases"][step]["commands"][str(com)]["output_format"])
        _mylog = self._addfile_(type="log", dir=step, step="step" + str(com),
                                  name=self.name + "_" + step + "_step" + str(com) + ".log")
        _myerror = self._addfile_(type="log", dir=step, step="step" + str(com),
                                    name=self.name + "_" + step + "_step" + str(com) + ".error")

        _myinput, _myinputformat, _myauxfile, _myauxformat = self._getinputinfo(step, com)
        if input is not None:
            _myinput = input
        _mycommand_ = str(pipeline["Phases"][step]["commands"][str(com)]["command"]).replace('_outputfile_', str(_myoutput))
        _mycommand_ = _mycommand_.replace('_aux_', str(_myauxfile))
        _mycommand_ = _mycommand_.replace('_auxformat_', str(_myauxformat))
        _mycommand_ = _mycommand_.replace('_log_', str(_mylog))
        _mycommand_ = _mycommand_.replace('_error_', str(_myerror))
        if pipeline["Phases"][step]["commands"][str(com)]["output_format"] == "dir":
            _mycommand_ = _mycommand_.replace('_outputdir_', str(_mydir + "/" + self.name + "_"))
        ### For paired-end and single-end data

        _myinput = _myinput.replace(';', ' ')
        _mycommand_ = _mycommand_.replace('_inputfile_', str(_myinput))
        _mycommand_ = _mycommand_.replace('_format_', str(_myinputformat))

        ### For paired-end data
        # if re.search(';', str(_myinput)) != None:
        #     _mycommand_ = _mycommand_.replace('_input_R1_', str(_myinput).split(';')[0])
        #     _mycommand_ = _mycommand_.replace('_input_R2_', str(_myinput).split(';')[1])
        #
        ### For single-end data
        # else:
        #     _mycommand_ = _mycommand_.replace('_inputfile_', str(_myinput))

        _mycommand_ = _mycommand_.replace('_name_', str(self.name))
        _mycommand_ = _mycommand_.replace('_ID_', str(self.ID))
        _mycommand_ = _mycommand_.replace('_capture_', str(self.capture_tech["kit"] + "_" + self.capture_tech["panel"]))
        _mycommand_ = _mycommand_.replace('_platform_', str(self.sequencer))
        _mycommand_ = _mycommand_.replace('_intervals_', str(pipeline["capture_files"]["covered"]))

        if "auxfile" in pipeline["Phases"][step]["commands"][str(com)]:
            self.lastfile["aux"] = _myoutput
            self.lastfile["auxformat"] = pipeline["Phases"][step]["commands"][str(com)]["output_format"]
        else:
            self.lastfile["name"] = _myoutput
            self.lastfile["format"] = pipeline["Phases"][step]["commands"][str(com)]["output_format"]
        return _mycommand_

class Sample(Essay):
    def __init__(self, name = None, json=None, path=None, state=None, command=None, pipeline=None):
        self.name = json["ID"]
        self.ID = name
        self.sequencer = pipeline["sequencer"]
        self.capture_tech = pipeline["capture_tech"]
        self.state = json["status"]
        self.species = json["species"]
        self.path = path
        self.files = {}
        self.lastfile = {'aux':'','auxformat':'','name':'','format':''}
        self.files = _addkeys_(self.files, keys=["inputfiles", "analysis", state, str(command)])
        self.files["inputfiles"]["analysis"][state][str(command)]["name"] = json["input_file"]
        self.files["inputfiles"]["analysis"][state][str(command)]["format"] = json["input_file_format"]
        self.pipeline = 'None'
        self._readstate_(state, command)

