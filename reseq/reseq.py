#!/usr/bin/env python
"""
SYNOPSIS
    reseq.py [-h,--help] [-v,--verbose] [--version]
DESCRIPTION
    Pipeline Analysis for Targetted DNA resequencing
EXAMPLES
    TODO:
EXIT STATUS
    TODO: List exit codes
AUTHOR
    Juan Manuel Rosa <juanm.rosa@gmail.com>
LICENSE
    This script belongs to Juan Manuel Rosa
VERSION
    reseq.py v0.1
"""
__author__ = 'jmrosa'

import sys
import getopt
import json
from tools import _gettime_
from essay import CreateEssay


def _main_(argv):
    if  not argv: print 'reseq.py -c <configfile>'
    try:
      opts, args = getopt.getopt(argv,"hc:",["cfile="])

    except getopt.GetoptError:
        print 'reseq.py -c <configfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'reseq.py -c <configfile>'
            sys.exit()
        elif opt in ("-c", "--cfile"):
            config_file = arg
            #print 'Config file is ', config_file

        for essay in _start_(config_file):
            print "Starting analysis of essay", essay.name, _gettime_()
            essay._writelog_("Process starts... " + _gettime_())
            print "My essay status is", str(essay.state["current"]["step"])
            exit = essay._launch_()
        if exit != "finished":
            essay._writelog_("<<ERROR>> while launching command in essay " + str(essay.name))
            essay._writelog_("<<ERROR>> in step " + str(essay.state["current"]["step"]))
            sys.exit(1)
        else:
            essay._writelog_("Process finished... " + _gettime_())

def _start_(config = None):
    with open (config, "r+") as f: _myjson = json.load(f)
    #print (json.dumps(_myjson, sort_keys=False, indent=4))
    _essays = []
    for n in range(1, len(_myjson["essays"].keys()) + 1):
        _myessay = CreateEssay(_myjson["essays"][str(n)], _myjson["path"], _myjson["essays"][str(n)]["name"])
        _essays.append(_myessay)
    return _essays

_main_(sys.argv[1:])
sys.exit(0)
